# dtkDiscreteGeometry Applicative Layer

## Introduction

This d-tk applicative layer is split into 3 parts : core, composer, and distributed.

The **core** of the layer provides an abstraction for each concept made available by the discrete geometry layer. It defines its input and ouput, and provides for each concept a virtual method to implement the process in the inheriting plugins.

These concepts are listed as follows :

*Reader* - *volumeMesher* - *volumeRemesher* - *surfaceMesher* - *surfaceRemesher* - *Optimizer* - *Repairer* - *Analyzer* - *Simplifier* - *Writer*

The **composer** part of the layer implements the transmission of objects between concepts, and achieves validity tests on the transmitted objects and on the concepts initialization.

The **distributed** part implements a distributed mesh based on the *dtkDistributed* layer

All the processes(*run* methods) regarding these different concepts must be defined in plugins, some examples are provided in [dtkDiscreteGeometryPlugins](https://github.com/d-tk/dtk-plugins-discrete-geometry).

More information regarding the d-tk philosophy can be found [here](http://dtk.inria.fr/guides/philosophy).

## Installation

### Prerequisites :
**Mandatory**
- [Qt5](https://www.qt.io/download/) : install or clone/build from sources
- [dtk](https://github.com/d-tk/dtk) : clone/build from sources

**Optional**
- [dtkImaging](https://github.com/d-tk/dtk-imaging) : clone/build from sources (for meshing application related to images)

### Layer installation :
- [dtkDiscreteGeometry](https://github.com/d-tk/dtk-discrete-geometry) : clone/build from sources
