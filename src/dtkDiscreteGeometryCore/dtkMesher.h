// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>
#include <QVariant>

#include <dtkCore>

class dtkMesh;
class dtkMeshCriterion;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMesher : public QRunnable
{
public:
    //TODO make it const : so we cant modify the pointer and what it points to ?
    virtual void setInputMesh(dtkMesh*) = 0;
    virtual void setInputImage(QString) = 0;
    virtual void setInputCAO(QString) = 0;

    virtual void setInputMeshCriterion(dtkMeshCriterion*) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh* outputDtkMesh(void) const = 0;
    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMesher*);
DTK_DECLARE_PLUGIN(dtkMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMesher, DTKDISCRETEGEOMETRYCORE_EXPORT, mesher);
}

//
// dtkMesher.h ends here
