// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>
#include <QVariant>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshStatisticsExtractor : public QRunnable
{
public:
    virtual void setMesh(const dtkMesh *mesh) = 0;

    virtual void run(void) = 0;

    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshStatisticsExtractor*);
DTK_DECLARE_PLUGIN(dtkMeshStatisticsExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshStatisticsExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshStatisticsExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshStatisticsExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT, meshStatisticsExtractor);
}

//
// dtkMeshStatisticsExtractor.h ends here
