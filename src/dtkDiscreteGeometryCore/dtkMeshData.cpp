// dtkMeshData.cpp ---
//
// Version:
//
//

// Commentary:
//
//

// Change log:
//
//


#include "dtkMeshData.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshData
  \inmodule dtkDiscreteGeometry
  \brief dtkMeshData is the data container of mesh informations


 */

/*! \enum dtkMeshData::CellType

    this enum is used to defined supported cells. the values directly come from VTK in order to easily be compliant.
    If you want to add a new cell type, you should also edit the array static quint32 nb_points_per_cell_type[25]

    \value Vertex
    \value Line
    \value Quad
    \value QuadLine
    \value Triangle
    \value Tetra
    \value Hexahedron
    \value Wedge
 */

/*!
   Constructor.
*/
dtkMeshData::dtkMeshData(const dtkMeshData& mesh_data)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(mesh_data);
}

/*!
   Clone
*/
dtkMeshData* dtkMeshData::clone(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return nullptr;
}

/*!
  returns the geometrical dimension of the mesh. usually 2 or 3
*/
int dtkMeshData::geometricalDimension(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return 0;
}
/*!
 sets the geometrical dimension of the mesh (in the Ambiant space) to \a geo_dim //WARNING for prototyping only
*/
void dtkMeshData::setGeometricalDimension(int geo_dim)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(geo_dim);
}

/*!
  returns the topological dimension of the mesh
*/
int dtkMeshData::topologicalDimension(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return 0;
}

/*!
 set the topological dimension of the mesh \a topo_dim //WARNING for prototyping only
 0 for a discrete topological space
 1 for a graph
 2 for a triangulated space
*/
void dtkMeshData::setTopologicalDimension(int topo_dim)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(topo_dim);
}

/*!
 copy the point coordinates of point \a id to \a point_coordinates . \a point coordinates is expected to be of
 dimension 3 for the plugin dtkMeshDataDefault.
*/
void dtkMeshData::pointCoordinates(dtkMeshData::IdxType id, double* point_coordinates) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(id);
    Q_UNUSED(point_coordinates);
}

/*!
 return a pointer to the array containing the point coordinates. the size of the array returned depend of the plugin (3*nb_points for dtkMeshDataDefault plugin even for 2D meshes).
*/
const double* dtkMeshData::pointsCoordinates(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return nullptr;
}

/*!
 return the number of points
*/
dtkMeshData::CntType dtkMeshData::pointsCount(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return 0;
}

/*!
 copy the point coordinates of point \a id from \a point_coordinates . \a point coordinates is expected to be of
 dimension  3 for the plugin dtkMeshDataDefault.
 you should call the method dtkMeshData::SetPointsCount(point_count) before calling this method.
*/
void dtkMeshData::setPoint(dtkMeshData::IdxType point_id, const double* points_coordinates)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(point_id);
    Q_UNUSED(points_coordinates);
}

/*!
  set the number of points to \a point_count .
 */
void dtkMeshData::setPointsCount(dtkMeshData::CntType point_count)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(point_count);
}

/*!
  set the points coordinates to \a point_coordinates. If \a points_count parameter is passed, the method will first call setPointsCount(points_count) before setting the points coordinates.
 */
void dtkMeshData::setPoints(const double* points_coordinates, dtkMeshData::CntType points_count)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(points_coordinates);
    Q_UNUSED(points_count);
}


/*!
 return the number of points of cell \a cell_id in \a out_nb_points_cell and set the pointer \a out_first_point to the position of the first point of this cell for the plugin dtkMeshDataDefault.
*/
void dtkMeshData::cellPoints(dtkMeshData::IdxType cell_id, int &out_nb_points_cell, const dtkMeshData::IdxType*& out_first_point) const// out nb points of cell , out_first_point pointer to the first point of the cell
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cell_id);
    Q_UNUSED(out_nb_points_cell);
    Q_UNUSED(out_first_point);
}

/*!
  return the cell type of the cell \a
*/
dtkMeshData::CellType dtkMeshData::cellType(dtkMeshData::IdxType cell_id) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cell_id);
    return dtkMeshData::Vertex;
}

/*!
 return the number of cells
*/
dtkMeshData::CntType dtkMeshData::cellsCount(int geo_dimension) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(geo_dimension);
    return 0;
}

/*!
 return a pointer of cells_idx corresponding to the cells of dimension \a geo_dimension . The number of cells returned is set in
 output parameter \a out_nb_cells
*/
const dtkMeshData::IdxType* dtkMeshData::cellsIdx(CntType& out_nb_cells, int geo_dimension) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(out_nb_cells);
    Q_UNUSED(geo_dimension);
    return nullptr;
}

/*!
 return a pointer of cells_points corresponding to the cells of dimension \a geo_dimension . The number of cells returned is set in
 output parameter \a out_nb_cells
*/
const dtkMeshData::IdxType* dtkMeshData::cellsPoints(CntType& out_nb_points, int geo_dimension) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(out_nb_points);
    Q_UNUSED(geo_dimension);
    return nullptr;
}

/*!
 return a pointer to an array defining the cell types of cells of dimension \a geo_dimension
*/
const dtkMeshData::CellType* dtkMeshData::cellsType(int geo_dimension) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(geo_dimension);
    return nullptr;
}

/*!
  return the cell topology location (only work for \a geo_dimension = 3 for dtkMeshDataDefault) of cells of dimension
  \a geo_dimension . you have to call setCellsType before.
  for dtkMeshDataDefault, cell_topology_3D location is an array of size nb_cells_3D with values index of this cells_points in the array returned by cellsPoints.
 */
const dtkMeshData::IdxType* dtkMeshData::cellsTopologyLocation(int geo_dimension) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(geo_dimension);
    return nullptr;
}

/*!
 * copies into \a out_points_ids the ids of the points of the cell of type \a cell_type, and id \a cell_id (in the type numerotation, ex : second tetrahedra)
 */
void dtkMeshData::typedCellPointsIds(const CellType cell_type, unsigned int cell_id, IdxType* out_points_ids) const {
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cell_type);
    Q_UNUSED(cell_id);
    Q_UNUSED(out_points_ids);
}

/*!
 set the number of cells for dimension \a geo_dimension [0-3] to \a cells_count .
 the total number of cells ( sum of the cells for all dimensions ) if automatically updated.
  dimension 0 : vertex
  dimension 1 : lines
  dimension 2 : surfaces ( triangle, quad, ..)
  dimension 3 : volumes (tetra, wedge, hexahedron, ..)
*/
void dtkMeshData::setCellsCount(dtkMeshData::CntType cells_count, int geo_dimension)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cells_count);
    Q_UNUSED(geo_dimension);
}

/*!
 set the cells type of \a cells_count contiguous cells starting from cell id \a offset to \a cells_type .
 if \a cells_count and \a offset aren't set then, the function will do a rawData affectation.
 the funciton will also create cell_topology_location.
*/
void dtkMeshData::setCellsType(const dtkMeshData::CellType* cells_type, dtkMeshData::CntType cells_count, CntType offset )
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cells_type);
    Q_UNUSED(cells_count);
    Q_UNUSED(offset);
}

/*!
 set the cells points of all the cells doint a SetRawData.
 dtkMeshDataDefault expect the following format of cells points for each cell : <nb_points_of_cell> , <point_0>, <point_1>, .. , <point_n>
 */
void dtkMeshData::setCellsPoints(const dtkMeshData::IdxType* cells_points, dtkMeshData::CntType size_cells_points)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(cells_points);
    Q_UNUSED(size_cells_points);
}

/*!
 append a new attribute
*/
void dtkMeshData::addAttribute(const dtkAttribute* attribute)
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(attribute);
}

/*!
 return the attribute with name \a name .if the attribute is not found, return nullptr
*/
const dtkAttribute* dtkMeshData::attribute(const QString& name) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(name);
    return nullptr;
}

/*!
 return all the attributes
*/
const dtkAttribute* dtkMeshData::attributes(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return nullptr;
}

/*!
 return the numbe of attributes
*/
dtkMeshData::CntType dtkMeshData::attributesCount(void) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    return 0;
}

/*!
 * returns the ids of all elements included in the element of dimension \a ele_dim at id \ele_id
 * if
 */
unsigned int* dtkMeshData::lower_degree_ele_ids(unsigned int ele_dim, const dtkMeshData::IdxType ele_id) const
{
    qDebug() << "default implementation of" << __func__ << "at " << __FILE__ << ":" << __LINE__;
    Q_UNUSED(ele_dim);
    Q_UNUSED(ele_id);
    return nullptr;
}


// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshData, meshData, dtkDiscreteGeometryCore);
}
