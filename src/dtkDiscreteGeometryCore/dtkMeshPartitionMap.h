// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <vector>

class QString;

// ///////////////////////////////////////////////////////////////////
// dtkMeshPartitionMap interface
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshPartitionMap
{
public:
     dtkMeshPartitionMap(void) = default;
    ~dtkMeshPartitionMap(void);

    void setPartitionCount(std::size_t partition_count);
    void setElementCount(std::size_t elmt_count);
    void setVertexCount(std::size_t vert_count);

    std::size_t partitionCount(void) const;
    std::size_t elementCount(void) const;
    std::size_t vertexCount(void) const;

    const long int *elementsPartition(void) const;
          long int *elementsPartition(void);

    const long int *verticesPartition(void) const;
          long int *verticesPartition(void);

public:
    bool  read(const QString& filename);
    bool write(const QString& filename) const;

private:
    std::size_t m_partition_count;

    std::vector<long int> m_elmt_parts;
    std::vector<long int> m_vert_parts;
};

// ///////////////////////////////////////////////////////////////////

inline void dtkMeshPartitionMap::setPartitionCount(std::size_t partition_count)
{
    m_partition_count = partition_count;
}

inline void dtkMeshPartitionMap::setElementCount(std::size_t element_count)
{
    m_elmt_parts.resize(element_count);
}

inline void dtkMeshPartitionMap::setVertexCount(std::size_t vertex_count)
{
    m_vert_parts.resize(vertex_count);
}

inline std::size_t dtkMeshPartitionMap::partitionCount(void) const
{
    return m_partition_count;
}

inline std::size_t dtkMeshPartitionMap::elementCount(void) const
{
    return m_elmt_parts.size();
}

inline const long int *dtkMeshPartitionMap::elementsPartition(void) const
{
    return m_elmt_parts.data();
}

inline long int *dtkMeshPartitionMap::elementsPartition(void)
{
    return m_elmt_parts.data();
}

inline std::size_t dtkMeshPartitionMap::vertexCount(void) const
{
    return m_vert_parts.size();
}

inline const long int *dtkMeshPartitionMap::verticesPartition(void) const
{
    return m_vert_parts.data();
}

inline long int *dtkMeshPartitionMap::verticesPartition(void)
{
    return m_vert_parts.data();
}

//
// dtkMeshPartitionMap.h ends here
