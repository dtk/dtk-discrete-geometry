// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMesh.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMesh
  \brief dtkMesh is a container of \l dtkMeshData.

  \inmodule dtkDiscreteGeometry

  long description

  \code
  dtkMesh *mesh = new dtkMesh(dtkDiscreteGeometry::meshData::pluginFactory().create("dtkMeshDataDefault");
  \endcode

 */

/*! \fn dtkMesh::dtkMesh(void)
   Constructor.
*/

/*! \fn dtkMesh::dtkMesh(dtkMeshData *engine)
   Constructor with a dtkMeshData
*/

/*! \fn dtkMesh::dtkMesh(const dtkMesh& other)
   Copy constructor
*/

/*! \fn dtkMesh::~dtkMesh(void)
   Destructor.
*/

/*! \fn dtkMeshData* dtkMesh::data(void)
   return a pointer to m_data
*/

/*! \fn const dtkMeshData* dtkMesh::data(void) const
   return a const pointer to m_data
*/

/*! \fn void dtkMesh::setData(dtkMeshData *data)
   set m_data
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMesh, mesh, dtkDiscreteGeometryCore);
}
