// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshSurfacesExtractor.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshSurfacesExtractor
  \brief The dtkMeshSurfacesExtractor class is an abstraction defining a mesh surfaces extractor, from a dtkMesh of topological dimension 3 and lower, it recovers the dimension 2 topological components.

  \since 1.0.0
  \ingroup extractors
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkMeshSurfacesExtractor::setMesh(const dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkMeshSurfacesExtractor::run(void) = 0
*/

/*!
  \fn virtual dtkMeshCollection *dtkMeshSurfacesExtractor::meshCollection(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshSurfacesExtractor, meshSurfacesExtractor, dtkDiscreteGeometryCore);
}

//
// dtkMeshSurfacesExtractor.cpp ends here
