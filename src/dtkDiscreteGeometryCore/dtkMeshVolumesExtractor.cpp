// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshVolumesExtractor.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshVolumesExtractor
  \brief The dtkMeshVolumesExtractor class is an abstraction defining a mesh surfaces extractor, from a dtkMesh of topological dimension 3 and lower, it recovers the dimension 3 topological components.

  \since 1.0.0
  \ingroup extractors
  \inmodule dtkDiscreteGeometry
*/

/*!
  \fn virtual void dtkMeshVolumesExtractor::setMesh(const dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkMeshVolumesExtractor::run(void) = 0
*/

/*!
  \fn virtual dtkMeshCollection* dtkMeshVolumesExtractor::meshCollection(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshVolumesExtractor, meshVolumesExtractor, dtkDiscreteGeometryCore);
}

//
// dtkMeshVolumesExtractor.cpp ends here
