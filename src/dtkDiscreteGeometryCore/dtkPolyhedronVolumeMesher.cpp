// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkPolyhedronVolumeMesher.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkPolyhedronVolumeMesher
  \brief The dtkPolyhedronVolumeMesher class is an abstraction defining a volumic mesher which discretizes a domain defined as a closed polyhedral surface into a polyelement mesh.

  \since 1.0.0
  \ingroup meshers
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkPolyhedronVolumeMesher::setMesh(dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkPolyhedronVolumeMesher::run(void) = 0
*/

/*!
  \fn virtual dtkMesh *dtkPolyhedronVolumeMesher::mesh(void) const = 0
*/

/*!
  \fn virtual QVariantMap dtkPolyhedronVolumeMesher::statistics(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkPolyhedronVolumeMesher, polyhedronVolumeMesher, dtkDiscreteGeometryCore);
}

//
// dtkPolyhedronVolumemesher.cpp ends here
