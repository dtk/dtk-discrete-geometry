// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshCollection.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshCollection
  \brief dtkMeshCollection is a list of pointers to /l dtkMesh.

  \code
	dtkMeshCollection *mesh_collection = new dtkMeshCollection();
  \endcode

  \inmodule dtkDiscreteGeometry
 */

/*!
    \fn dtkMeshCollection::dtkMeshCollection(void)
    Constructor.
*/

/*!
    \fn dtkMeshCollection::~dtkMeshCollection(void)
    Destructor.
*/

/*!
    \fn inline dtkMeshCollection& dtkMeshCollection::operator=(const dtkMeshCollection& other)
    operator =
*/

/*!
   \fn  inline dtkMesh*& dtkMeshCollection::operator[](unsigned int key)
   Adds or access a /l dtkMesh to or from a /l dtkMeshCollection using a key
*/

/*!
    \fn inline int dtkMeshCollection::remove(unsigned int key)
    Removes a /l dtkMesh from the /l dtkMeshCollection
*/

/*!
    \fn inline QList<dtkMesh*>* dtkMeshCollection::values() const
    return a /l QList containing all the /l dtkMesh of the /l dtkCollection
*/


// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshCollection, meshCollection, dtkDiscreteGeometryCore);
}
