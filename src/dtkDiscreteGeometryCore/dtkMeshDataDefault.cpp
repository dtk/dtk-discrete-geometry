// dtkMeshDataDefault.cpp ---
//
// Version:
//
//

// Commentary:
//
//

// Change log:
//
//

#include <dtkCore>

#include "dtkMeshDataDefault.h"

class dtkMeshDataDefaultPrivate
{

};

dtkMeshDataDefault::dtkMeshDataDefault(void)
{
     m_geo_dim=-1;//equivalent to unset
     m_topo_dim=-1;//equivalent to unset
     m_nb_points=0;
     m_nb_cells[0]=m_nb_cells[1]=m_nb_cells[2]=m_nb_cells[3]=0;
     m_attributes.reserve(5);

}


dtkMeshDataDefault::dtkMeshDataDefault(const dtkMeshDataDefault& other)
{
     m_nb_cells[0] = other.m_nb_cells[0];
     m_nb_cells[1] = other.m_nb_cells[1];
     m_nb_cells[2] = other.m_nb_cells[2];
     m_nb_cells[3] = other.m_nb_cells[3];
     m_nb_cells[4] = other.m_nb_cells[4];

     m_geo_dim = other.m_geo_dim;
     m_topo_dim = other.m_topo_dim;

     m_points_coordinates = other.m_points_coordinates;
     m_cells_type = other.m_cells_type;
     m_cells_first_point_idx = other.m_cells_first_point_idx;
     m_cells_points = other.m_cells_points;
     m_attributes = other.m_attributes;

     m_nb_points = other.m_nb_points;

}
dtkMeshDataDefault::~dtkMeshDataDefault(void)
{
     m_points_coordinates.clear();
     m_cells_type.clear();
     m_cells_first_point_idx.clear();
     m_cells_points.clear();
     m_attributes.clear();
}

dtkMeshDataDefault* dtkMeshDataDefault::clone(void) const
{
    return new dtkMeshDataDefault(*this);
}

int dtkMeshDataDefault::geometricalDimension(void) const
{
    return m_geo_dim;
}

void  dtkMeshDataDefault::setGeometricalDimension(int geo_dim)
{
        if(geo_dim<=0){
                dtkFatal() << "Dimension can't be set negative";
        }
        m_geo_dim = geo_dim;
}

int dtkMeshDataDefault::topologicalDimension(void) const
{
    return m_topo_dim;
}

void  dtkMeshDataDefault::setTopologicalDimension(int topo_dim)
{
        if(topo_dim<=0){
                dtkFatal() << "Dimension can't be set negative";
        }
    m_topo_dim = topo_dim;
}

//WARNING it is assumed that point_coordinates is an array of size geometrical_dimension
void  dtkMeshDataDefault::pointCoordinates(dtkMeshData::IdxType id, double* points_coordinates) const
{
    Q_ASSERT(id<m_nb_points);

    points_coordinates[0] =  m_points_coordinates[3*id];
    points_coordinates[1] =  m_points_coordinates[3*id+1];
    if( m_geo_dim==3 ) points_coordinates[2] = m_points_coordinates[3*id+2];
}

const double* dtkMeshDataDefault::pointsCoordinates(void) const
{
    return m_points_coordinates.data();
}

dtkMeshData::CntType dtkMeshDataDefault::pointsCount(void) const
{
    return m_nb_points;
}

void dtkMeshDataDefault::setPoint(dtkMeshData::IdxType point_id, const double* points_coordinates)
{
    Q_ASSERT(point_id<m_nb_points);

    m_points_coordinates[3*point_id]=points_coordinates[0];
    m_points_coordinates[3*point_id+1]=points_coordinates[1];
    m_points_coordinates[3*point_id+2]= ((m_geo_dim==3)? points_coordinates[2] : 0.0);
}

void dtkMeshDataDefault::setPointsCount(CntType point_count)
{
    // if new size greater, do a resize
    if(point_count> m_nb_points)
        m_points_coordinates.resize(3*point_count);

    m_nb_points = point_count;
}

// rawData copy
void dtkMeshDataDefault::setPoints(const double* points_coordinates, dtkMeshData::CntType points_count)
{
    if(points_count) {
        setPointsCount(points_count);
//         m_points_coordinates.setRawData(points_coordinates, points_count*(m_dim_3d? 3 : 2));
        dtkMeshData::CntType coordiantes_count = points_count*((m_geo_dim==3)? 3 : 2);
                m_points_coordinates.resize(coordiantes_count);
        std::copy(points_coordinates, points_coordinates + coordiantes_count, m_points_coordinates.begin());
    }
    else
//         m_points_coordinates.setRawData(points_coordinates, m_nb_points*(m_dim_3d? 3 : 2));
        std::copy(points_coordinates, points_coordinates + m_nb_points*((m_geo_dim==3)? 3 : 2), m_points_coordinates.begin());
}

void dtkMeshDataDefault::cellPoints(IdxType cell_id, int &out_nb_points_cell, const dtkMeshData::IdxType*& out_first_point) const  // out nb points of cell , out_first_point pointer to the first point of the cell
{
    Q_ASSERT(cell_id <  m_nb_cells[4]);

    out_nb_points_cell = m_cells_points[m_cells_first_point_idx[cell_id]];
    out_first_point = &m_cells_points[m_cells_first_point_idx[cell_id]];
    ++out_first_point;
}

dtkMeshData::CellType dtkMeshDataDefault::cellType(dtkMeshData::IdxType cell_id) const
{
    return  m_cells_type[cell_id];
}

dtkMeshData::CntType dtkMeshDataDefault::cellsCount(int geo_dimension) const
{
    return  m_nb_cells[geo_dimension];
}

const dtkMeshData::IdxType* dtkMeshDataDefault::cellsIdx(dtkMeshData::CntType& out_nb_cells, int geo_dimension) const
{
    Q_ASSERT(geo_dimension>=0 && geo_dimension<5);

    dtkMeshData::CntType idx = 0;

    if( geo_dimension > 0)
        idx += m_nb_cells[0];

    if( geo_dimension > 1)
        idx += m_nb_cells[1];

    if(geo_dimension > 2)
        idx += m_nb_cells[2];

    if(geo_dimension > 3)
        idx=0;

    out_nb_cells = m_nb_cells[geo_dimension];

    return &m_cells_first_point_idx[idx];
}

//return format  (nb_points, point_0, point_1, .., point_{nb_points-1} ) for each cel . out_nb_points = out_size = nb_points + 1*nb_cells_out
const dtkMeshData::IdxType* dtkMeshDataDefault::cellsPoints(dtkMeshData::CntType& out_nb_points, int geo_dimension) const
{
    Q_ASSERT(geo_dimension>=0 && geo_dimension<5);

    dtkMeshData::CntType out_idx = 0; //index in m_cells_first_point_idx of the first cell of queried dimension
    for (int i=0; i<geo_dimension; ++i)
    {
        out_idx += m_nb_cells[i];
    }

    dtkMeshData::CntType idx_dim_supp; //index in m_cells_first_point)idx of the first cell of (queried dimension +1)

    //compute idx dim_supp
    if(geo_dimension<3) {
        idx_dim_supp = out_idx + m_nb_cells[geo_dimension];
        out_nb_points = m_cells_first_point_idx[idx_dim_supp] - m_cells_first_point_idx[out_idx] ;
    }
    else {
        //for dimension 3 or for the total
        idx_dim_supp =  m_cells_first_point_idx.count()-1;
        if(geo_dimension == 4)
            out_idx=0;

        out_nb_points = m_cells_first_point_idx[idx_dim_supp] - m_cells_first_point_idx[out_idx] ;
    }
    return &m_cells_points[m_cells_first_point_idx[out_idx]];
}

const dtkMeshData::CellType* dtkMeshDataDefault::cellsType(int geo_dimension) const
{
    Q_ASSERT(geo_dimension>=0 && geo_dimension < 5);

    if(geo_dimension == 4) {
        return  m_cells_type.data();
    }


    dtkMeshData::CntType out_idx = 0;
    for (int i=0; i<geo_dimension; ++i)
    {
        out_idx += m_nb_cells[i];
    }

    return &m_cells_type[out_idx];
}

const dtkMeshData::IdxType* dtkMeshDataDefault::cellsTopologyLocation(int geo_dimension) const
{
    Q_ASSERT(geo_dimension>=0 && geo_dimension < 5);

    if(geo_dimension == 4) {
        return  m_cells_first_point_idx.data();
    }


    dtkMeshData::CntType out_idx = 0;
    for (int i=0; i<geo_dimension; ++i)
    {
        out_idx += m_nb_cells[i];
    }

    return &m_cells_first_point_idx[out_idx];
}

unsigned int dtkMeshDataDefault::facet_corner_id(const dtkMeshData::IdxType face_id, const dtkMeshData::IdxType corner_id) const{
        return m_cells_points[2*m_nb_cells[0]+3*m_nb_cells[1]+4*(face_id)+1+corner_id];
}

void dtkMeshDataDefault::setCellsCount(dtkMeshData::CntType cells_count, int geo_dimension)
{
    Q_ASSERT(geo_dimension >= 0  && geo_dimension<4);

    if( m_nb_cells[geo_dimension] < cells_count) {
        m_nb_cells[geo_dimension] = cells_count;
        m_nb_cells[4] =  m_nb_cells[0] + m_nb_cells[1] + m_nb_cells[2] + m_nb_cells[3];

        //do a realloc
        m_cells_type.resize(m_nb_cells[4]);
        m_cells_first_point_idx.resize( m_nb_cells[4]+1);
    }
    else {
        m_nb_cells[geo_dimension] = cells_count;
        m_nb_cells[4] = m_nb_cells[0] + m_nb_cells[1] + m_nb_cells[2] + m_nb_cells[3];
    }
}

void dtkMeshDataDefault::setCellsType(const dtkMeshData::CellType* cells_type, dtkMeshData::CntType cells_count, dtkMeshData::CntType offset)
{
    if(cells_count == 0) {
//          m_cells_type.setRawData(cells_type,  m_nb_cells[4]);
         std::copy(cells_type, cells_type + m_nb_cells[4], m_cells_type.begin());
    }
    else {
        if((cells_count+offset) <= m_nb_cells[4]) {
            for(long long int i=0; i<cells_count; ++i)
            {
                 m_cells_type[i+offset] = cells_type[i];
            }
        }
        else
            dtkError() << __func__ << "nb_cells[4]=" << m_nb_cells[4] << "cells_count+offset=" << cells_count+offset;
    }

    //fill m_cells_first_point_idx
    dtkMeshData::IdxType current_index=m_cells_first_point_idx[offset];
    for(dtkMeshData::CntType idx=offset; idx<m_nb_cells[4]; ++idx)
    {
        m_cells_first_point_idx[idx] = current_index;
        current_index += nb_points_per_cell_type[m_cells_type[idx]] + 1; //+1 since for each cell, there is (nb_points, point_0, point_1, .., point{nb_points-1} )
    }
    // extra entry for last value
    m_cells_first_point_idx[m_nb_cells[4]] = current_index-1;
}

void dtkMeshDataDefault::setCellsPoints(const dtkMeshData::IdxType* cells_points, dtkMeshData::CntType size_cells_points)
{
//     m_cells_points.setRawData(cells_points, size_cells_points);
    m_cells_points.resize(size_cells_points);
    std::copy(cells_points, cells_points + size_cells_points, m_cells_points.begin());
}

void dtkMeshDataDefault::addAttribute(const dtkAttribute * attribute)
{
    m_attributes.append(*attribute);
}


const dtkAttribute* dtkMeshDataDefault::attribute(const QString& name) const
{
    for (auto i=0u; i< m_attributes.length(); ++i)
    {
        if(m_attributes[i].m_name == name)
            return &m_attributes[i];
    }

    return nullptr;
}

const dtkAttribute* dtkMeshDataDefault::attributes(void) const
{
    return m_attributes.data();
}

dtkMeshData::CntType dtkMeshDataDefault::attributesCount(void) const
{
    return m_attributes.size();
}
