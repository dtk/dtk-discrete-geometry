// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshReader.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshReader
  \brief The dtkMeshReader class is an abstraction defining a mesh file reader.

  \since 1.0.0
  \ingroup readers
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkMeshReader::setMeshFile(const QString&) = 0
*/

/*!
  \fn virtual void dtkMeshReader::run(void) = 0
*/

/*!
  \fn virtual dtkMesh *dtkMeshReader::mesh(void) const = 0
*/

/*!
  \fn   virtual const QStringList& extensions(void) const = 0
  Returns the list of the extensions handled by the mesh reader.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshReader, meshReader, dtkDiscreteGeometryCore);
}

//
// dtkMeshReader.cpp ends here
