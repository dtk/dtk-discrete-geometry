// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>
#if dtk_VERSION_MAJOR == 1
#  include <dtkMeta>
#else // dtk-2.x or later
#  include <dtkCoreMetaType>
#  include <dtkArray.h>
#endif

#include <dtkDiscreteGeometryCoreExport.h>
#if dtk_VERSION_MAJOR != 1
#  define dtkArray dtk::ArrayFinal
#endif
// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkAttribute
{
public:
    dtkAttribute() : m_rank(-1), m_name(""), m_kind(0), m_support(0) {;}
    dtkAttribute(const int& rank, const QString& name, const unsigned char& kind, const unsigned char& support,const dtkArray<double>& data) : m_rank(rank), m_name(name), m_kind(kind), m_support(support), m_data(data) {;}
    ~dtkAttribute() {;}

public:
    dtkAttribute(const dtkAttribute& other) {
        m_rank = other.m_rank;
        m_name = other.m_name;
        m_kind = other.m_kind;
        m_support = other.m_support;
        m_data = other.m_data;
    }

public:
    int m_rank; //mpi_rank
    QString m_name;
    unsigned char m_kind; // 1 scalar, 3 vector, 9 tensor
    unsigned char m_support; // 1 = point, 2 = cell, other = custom
    dtkArray<double> m_data;
};


#if dtk_VERSION_MAJOR != 1 // dtk-2.x or later
template <typename T> inline QDebug& operator << (QDebug debug, const dtkArray<T>& array)
{
    const bool oldSetting = debug.autoInsertSpaces();
    debug.nospace() << "dtkArray";
    debug.nospace() << '(';

    for (typename dtkArray<T>::size_type i = 0; i < array.size(); ++i) {
        if (i)
            debug << ", ";

        debug << array.at(i);
    }

    debug << ')';
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();
}

template<typename T> inline QDataStream& operator << (QDataStream& s, const dtkArray<T>& array)
{
    s << quint64(array.size());

    for (const T& t : array)
        s << t;

    return s;
}


template<typename T> inline QDataStream& operator >> (QDataStream& s, dtkArray<T>& array)
{
    array.clear();
    quint64 size; s >> size;
    array.resize(size);

    for (T& t : array) {
        s >> t;
    }

    return s;
}
#endif // dtk-2.x or later

inline QDebug operator<<(QDebug dbg, const dtkAttribute& data) {
    dbg.nospace() << data.m_rank  <<  data.m_name << "kind:" << data.m_kind << "support:" << data.m_support << "data:" << data.m_data;
    return dbg.space();
}
inline QDebug operator<<(QDebug dbg, dtkAttribute *data) {
    dbg.nospace() << data->m_rank  <<  data->m_name << "kind:" << data->m_kind << "support:" << data->m_support << "data:" << data->m_data;;
    return dbg.space();
}

inline QDataStream& operator<<(QDataStream& s, const dtkAttribute& data) {
    s << data.m_rank;
    s << data.m_name;
    s << data.m_kind;
    s << data.m_support ;
    s << data.m_data;
    return s;
}

inline QDataStream& operator>>(QDataStream& s, dtkAttribute& data)       {
    s >> data.m_rank;
    s >> data.m_name;
    s >> data.m_kind;
    s >> data.m_support ;
    s >> data.m_data;
    return s;
}

// ///////////////////////////////////////////////////////////////////
// Declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAttribute*)

//
// dtkAttribute.h ends here
