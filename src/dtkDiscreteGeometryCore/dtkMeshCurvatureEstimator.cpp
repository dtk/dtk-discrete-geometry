// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshCurvatureEstimator.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshCurvatureEstimator
  \brief The dtkMeshCurvatureEstimator class is an abstraction defining a mesh curvature computation thanks to the  estimator for a dtkMesh of topological dimension 2.

  \since 1.0.0
  \ingroup estimators
  \inmodule dtkDiscreteGeometry
*/

/*!
  \fn void dtkMeshCurvatureEstimator::setMesh(dtkMesh *mesh)
*/

/*!
  \fn void dtkMeshCurvatureEstimator::run(void)
*/

/*!
  \fn dtkMesh* mesh(void) const
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshCurvatureEstimator, meshCurvatureEstimator, dtkDiscreteGeometryCore);
}

//
// dtkMeshCurvatureEstimator.cpp ends here
