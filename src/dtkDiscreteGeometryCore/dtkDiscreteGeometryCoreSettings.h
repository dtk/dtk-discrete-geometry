// Version: $Id: a13b573c599de67d9197ee11d9de2078709423e5 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QtCore>

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkDiscreteGeometryCoreSettings : public QSettings
{
public:
     dtkDiscreteGeometryCoreSettings(void);
    ~dtkDiscreteGeometryCoreSettings(void);
};

//
// dtkDiscreteGeometrySettings.h ends here
