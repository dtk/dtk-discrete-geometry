// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshesComparator.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkMeshesComparator
  \brief The dtkMeshesComparator class is an abstraction defining a meshes comparator.

  \since 1.0.0
  \ingroup meshesComparators
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkMeshesComparator::setMeshes(const dtkMesh *mesh_a, const dtkMesh *mesh_b) = 0
*/

/*!
  \fn virtual void dtkMeshesComparator::run(void) = 0
*/

/*!
  \fn virtual QVariantMap dtkMeshesComparator::statistics(void) const = 0
*/

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshesComparator, meshesComparator, dtkDiscreteGeometryCore);
}

//
// dtkMeshesComparator.cpp ends here
