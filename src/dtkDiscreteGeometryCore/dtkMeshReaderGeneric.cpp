// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshReaderGeneric.h"

#include <dtkDiscreteGeometryCore.h>
#include <dtkDiscreteGeometryCoreSettings.h>

#include <dtkMesh.h>

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// dtkMeshReaderGeneric implementation
// ///////////////////////////////////////////////////////////////////

dtkMeshReaderGeneric::dtkMeshReaderGeneric(void) : dtkMeshReader(), m_dtk_mesh(nullptr)
{
    QStringList plugins = dtkDiscreteGeometryCore::meshReader::pluginFactory().keys();
    for(auto i = 0; i < plugins.size(); ++i) {
        dtkMeshReader *mesh_reader = dtkDiscreteGeometryCore::meshReader::pluginFactory().create(plugins.at(i));
        m_mesh_readers.push_back(mesh_reader);
        m_extensions << mesh_reader->extensions();
    }
    m_extensions.removeDuplicates();
}

dtkMeshReaderGeneric::~dtkMeshReaderGeneric(void)
{
}

void dtkMeshReaderGeneric::setMeshFile(const QString& file)
{
    m_in_mesh_file = file;
}

void dtkMeshReaderGeneric::run(void)
{
    bool read = false;
    for (auto mesh_reader = m_mesh_readers.begin(); mesh_reader != m_mesh_readers.end(); ++mesh_reader) {
        if ((*mesh_reader)->extensions().indexOf(QFileInfo(m_in_mesh_file).suffix()) != -1){
            (*mesh_reader)->setMeshFile(m_in_mesh_file);
            (*mesh_reader)->run();
            m_dtk_mesh = (*mesh_reader)->mesh();
            read = true;
            break;
        }
    }
    if (!read) {
        dtkWarn() << "The files could not be read, make sure it is one of the following formats :  " << m_extensions;
    }
}

dtkMesh* dtkMeshReaderGeneric::mesh(void) const
{
    if (m_dtk_mesh == nullptr) {
        dtkWarn() << "The mesh was not generated, make sure you ran the \"run\" method, and that the extension is valid by checking the \"extensions\" method";
    }
    return m_dtk_mesh;
}

const QStringList& dtkMeshReaderGeneric::extensions(void) const
{
    return m_extensions;
}
//
// dtkMeshReaderGeneric.cpp ends here
