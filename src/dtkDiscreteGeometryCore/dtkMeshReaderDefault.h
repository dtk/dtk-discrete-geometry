// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include "dtkMeshReader"

class dtkMeshReaderDefaultPrivate;

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshReaderDefault : public dtkMeshReader
{
public:
     dtkMeshReaderDefault(void);
    ~dtkMeshReaderDefault(void);

public:
    void  setMeshFile(const QString&) final;

    void run(void) final;

    dtkMesh* mesh(void) const final;

    const QStringList& extensions(void) const final;

protected:
    dtkMeshReaderDefaultPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkMeshReader* dtkMeshReaderDefaultCreator(void)
{
    return new dtkMeshReaderDefault();
}

//
// dtkMeshReaderDefault.h ends here
