// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QtCore>

#include <dtkCore>

class dtkBRep;
class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkBrepVolumeMesher : public QRunnable
{
public:
    virtual void setBrep(const dtkBRep* brep) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh* mesh(void) const = 0;
    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkBrepVolumeMesher*);
DTK_DECLARE_PLUGIN(        dtkBrepVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkBrepVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkBrepVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkBrepVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT, brepVolumeMesher);
}

//
// dtkBrepVolumeMesher.h ends here
