// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshWriter.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshWriter, meshWriter, dtkDiscreteGeometryCore);
}

//
// dtkMeshWriter.cpp ends here
