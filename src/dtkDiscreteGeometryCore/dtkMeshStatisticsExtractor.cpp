// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshStatisticsExtractor.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshStatisticsExtractor
  \brief The dtkMeshStatisticsExtractor class is an abstraction defining a statistical extractor, which can compute all kind of statistics on the /l dtkMesh.

  \since 1.0.0
  \ingroup statisticsExtractors
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkMeshStatisticsExtractor::setMesh(const dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkMeshStatisticsExtractor::run(void) = 0
*/

/*!
  \fn virtual QVariantMap dtkMeshStatisticsExtractor::statistics(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshStatisticsExtractor, meshStatisticsExtractor, dtkDiscreteGeometryCore);
}

//
// dtkMeshStatisticsExtractor.cpp ends here
