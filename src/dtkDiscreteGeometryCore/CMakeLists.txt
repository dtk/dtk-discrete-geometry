## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkDiscreteGeometryCore)

## #################################################################
## Input
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkAttribute
  dtkAttribute.h
  dtkBrepVolumeMesher
  dtkBrepVolumeMesher.h
  dtkDiscreteGeometryCore
  dtkDiscreteGeometryCore.h
  dtkDiscreteGeometryCoreSettings
  dtkDiscreteGeometryCoreSettings.h
  dtkImageVolumeMesher
  dtkImageVolumeMesher.h
  dtkMesh
  dtkMesh.h
  dtkMeshCollection
  dtkMeshCollection.h
  dtkMeshData
  dtkMeshData.h
  dtkMeshDataDefault
  dtkMeshDataDefault.h
  dtkMesher
  dtkMesher.h
  dtkMeshesComparator
  dtkMeshesComparator.h
  dtkMeshLabeler
  dtkMeshLabeler.h
  dtkMeshCurvatureEstimator
  dtkMeshCurvatureEstimator.h
  dtkMeshNodeRenumbering
  dtkMeshNodeRenumbering.h
  dtkMeshOptimizer
  dtkMeshOptimizer.h
  dtkMeshPartitioner
  dtkMeshPartitioner.h
  dtkMeshPartitionMap
  dtkMeshPartitionMap.h
  dtkMeshReader
  dtkMeshReader.h
  dtkMeshReaderDefault
  dtkMeshReaderDefault.h
  dtkMeshReaderGeneric
  dtkMeshReaderGeneric.h
  dtkMeshStatisticsExtractor
  dtkMeshStatisticsExtractor.h
  dtkMeshSurfacesExtractor
  dtkMeshSurfacesExtractor.h
  dtkMeshView
  dtkMeshView.h
  dtkMeshVolumesExtractor
  dtkMeshVolumesExtractor.h
  dtkMeshWriter
  dtkMeshWriter.h
  dtkPolyhedronVolumeMesher
  dtkPolyhedronVolumeMesher.h)

set(${PROJECT_NAME}_SOURCES
  dtkBrepVolumeMesher.cpp
  dtkDiscreteGeometryCore.cpp
  dtkDiscreteGeometryCoreSettings.cpp
  dtkImageVolumeMesher.cpp
  dtkMesh.cpp
  dtkMeshCollection.cpp
  dtkMeshData.cpp
  dtkMeshDataDefault.cpp
  dtkMesher.cpp
  dtkMeshesComparator.cpp
  dtkMeshLabeler.cpp
  dtkMeshCurvatureEstimator.cpp
  dtkMeshNodeRenumbering.cpp
  dtkMeshOptimizer.cpp
  dtkMeshPartitioner.cpp
  dtkMeshPartitionMap.cpp
  dtkMeshReader.cpp
  dtkMeshReaderDefault.cpp
  dtkMeshReaderGeneric.cpp
  dtkMeshStatisticsExtractor.cpp
  dtkMeshSurfacesExtractor.cpp
  dtkMeshView.cpp
  dtkMeshVolumesExtractor.cpp
  dtkMeshWriter.cpp
  dtkPolyhedronVolumeMesher.cpp)

## #################################################################
## Wrapping
## #################################################################

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${dtk_MODULE_PATH})

set(${PROJECT_NAME}_SOURCES_WRAP)

if(DTK_BUILD_WRAPPERS)

  include(dtkWrap)

  set(${PROJECT_NAME}_WRAP_DEPENDS ${${PROJECT_NAME}_HEADERS} ${CMAKE_BINARY_DIR}/dtkDiscreteGeometryCore.i)

  if (DTK_WRAPPING_PYTHON AND DTK_PYTHON_WRAPPER MATCHES "SWIG")
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/dtkDiscreteGeometryCore.i.in ${CMAKE_BINARY_DIR}/dtkDiscreteGeometryCore.i @ONLY)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/dtkDiscreteGeometryCore.i.in ${CMAKE_SOURCE_DIR}/wrp/dtkDiscreteGeometryCore.i @ONLY)
    dtk_wrap(${PROJECT_NAME} ${PROJECT_NAME}_SOURCES_WRAP dtkdiscretegeometrycore python ${CMAKE_BINARY_DIR}/dtkDiscreteGeometryCore.i "${${PROJECT_NAME}_WRAP_DEPENDS}" "${dtk_INCLUDE_DIRS};${dtk_WRAP_PREFIX}/dtkBase;${dtk_WRAP_PREFIX}/dtkCore;${dtk_WRAP_PREFIX}/QtCore;${CMAKE_SOURCE_DIR}/src/dtkDiscreteGeometryCore;${CMAKE_SOURCE_DIR}/src")
    set_property(SOURCE dtkDiscreteGeometryCore_wrap_python.cpp PROPERTY SKIP_AUTOMOC ON)
  endif(DTK_WRAPPING_PYTHON AND DTK_PYTHON_WRAPPER MATCHES "SWIG")

endif(DTK_BUILD_WRAPPERS)

## #################################################################
## Build rules
## #################################################################

add_library(${PROJECT_NAME} SHARED
  ${${PROJECT_NAME}_HEADERS}
  ${${PROJECT_NAME}_SOURCES_WRAP}
  ${${PROJECT_NAME}_SOURCES})

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} PUBLIC Qt5::Core)
target_link_libraries(${PROJECT_NAME} PUBLIC dtkCore)
if(dtkCore_VERSION VERSION_GREATER 1.7)
  target_link_libraries(${PROJECT_NAME} PUBLIC dtkContainers)
endif()

## #################################################################
## Target properties
## #################################################################

set_target_properties(${PROJECT_NAME} PROPERTIES CXX_VISIBILITY_PRESET hidden)
set_target_properties(${PROJECT_NAME} PROPERTIES VISIBILITY_INLINES_HIDDEN 1)
set_target_properties(${PROJECT_NAME} PROPERTIES MACOSX_RPATH 0)
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH    "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${dtkDiscreteGeometry_VERSION}
                                                 SOVERSION ${dtkDiscreteGeometry_VERSION_MAJOR})

## #################################################################
## Handling of generated script modules
## #################################################################

if(DTK_BUILD_WRAPPERS)

  set(${PROJECT_NAME}_MODULES)

  if(DTK_WRAPPING_PYTHON AND PYTHONLIBS_FOUND AND DTK_PYTHON_WRAPPER MATCHES "SWIG")

    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
      DEPENDS ${PROJECT_SOURCE_DIR}/dtkDiscreteGeometryCore.i
      COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${CMAKE_BINARY_DIR}/modules
      COMMAND ${CMAKE_COMMAND} ARGS -E copy_if_different ${${PROJECT_NAME}_BINARY_DIR}/dtkdiscretegeometrycore.py ${CMAKE_BINARY_DIR}/modules
      COMMENT "-- Moving python modules to ${CMAKE_BINARY_DIR}/modules")

    set(${PROJECT_NAME}_MODULES ${CMAKE_BINARY_DIR}/modules/dtkdiscretegeometrycore.py)

    if(APPLE)
      add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        DEPENDS ${PROJECT_SOURCE_DIR}/dtkDiscreteGeometryCore.i
        COMMAND ln -sf ../lib/libdtkDiscreteGeometryCore.dylib _dtkdiscretegeometrycore.so
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/modules)

      set(${PROJECT_NAME}_MODULES ${${PROJECT_NAME}_MODULES} ${CMAKE_BINARY_DIR}/modules/_dtkdiscretegeometrycore.so)
    endif(APPLE)

    if(UNIX AND NOT APPLE)
      add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        DEPENDS ${PROJECT_SOURCE_DIR}/dtkDiscreteGeometryCore.i
        COMMAND ln -fs ../${CMAKE_INSTALL_LIBDIR}/libdtkDiscreteGeometryCore.so _dtkdiscretegeometrycore.so
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/modules)

      set(${PROJECT_NAME}_MODULES ${${PROJECT_NAME}_MODULES} ${CMAKE_BINARY_DIR}/modules/_dtkdiscretegeometrycore.so)
    endif(UNIX AND NOT APPLE)

  endif(DTK_WRAPPING_PYTHON AND PYTHONLIBS_FOUND AND DTK_PYTHON_WRAPPER MATCHES "SWIG")

endif(DTK_BUILD_WRAPPERS)

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export"
 "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(FILES ${${PROJECT_NAME}_HEADERS}
  DESTINATION include/dtkDiscreteGeometryCore
  COMPONENT discrete-geometry-core)

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME}
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
      COMPONENT discrete-geometry-core
         EXPORT dtkDiscreteGeometryDepends)

## ###################################################################
## Install rules - modules
## ###################################################################

if(DTK_BUILD_WRAPPERS)
  if(DTK_WRAPPING_PYTHON AND PYTHONLIBS_FOUND AND DTK_PYTHON_WRAPPER MATCHES "SWIG")
    install(FILES ${${PROJECT_NAME}_MODULES} DESTINATION modules)
    install(FILES ${CMAKE_BINARY_DIR}/dtkDiscreteGeometryCore.i DESTINATION wrp/dtkDiscreteGeometryCore/)
  endif(DTK_WRAPPING_PYTHON AND PYTHONLIBS_FOUND AND DTK_PYTHON_WRAPPER MATCHES "SWIG")
endif(DTK_BUILD_WRAPPERS)

######################################################################
### CMakeLists.txt ends here
