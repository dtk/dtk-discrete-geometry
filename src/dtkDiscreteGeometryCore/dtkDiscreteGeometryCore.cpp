// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#include "dtkDiscreteGeometryCore.h"
#include "dtkDiscreteGeometryCoreSettings.h"

#include "dtkDiscreteGeometryConfig.h"

#include "dtkMeshData"
#include "dtkMeshDataDefault.h"

#include "dtkMeshReader"
#include "dtkMeshReaderDefault.h"
#include "dtkMeshReaderGeneric.h"

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// Layer methods implementations
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {

DTK_DEFINE_LAYER_MANAGER;

    void initialize(const QString& path)
    {
        QString realpath = path;
        QStringList pathslist;
        if (path.isEmpty()) {
            dtkDiscreteGeometryCoreSettings geo_settings;
            geo_settings.beginGroup("discrete-geometry-core");
            realpath = geo_settings.value("plugins").toString();
            geo_settings.endGroup();
            //if (realpath.isEmpty()) {
            //    realpath = QDir(DTK_INSTALL_PREFIX).filePath("plugins/dtkDiscreteGeometry");
            //    dtkDebug() << "no plugin path configured for dtkDiscreteGeometry, use default:" << realpath ;
            //}
            pathslist = realpath.split(":");
        }
        else {
            pathslist = realpath.split(":");
        }
        foreach (const QString &v_path, pathslist) {
            manager().initialize(v_path);
        }

        // ///////////////////////////////////////////////////////////////////
        // Initializes default plugins
        // ///////////////////////////////////////////////////////////////////
        dtkDiscreteGeometryCore::meshData::pluginFactory().record("dtkMeshDataDefault", dtkMeshDataDefaultCreator);
        dtkDiscreteGeometryCore::meshReader::pluginFactory().record("dtkMeshReaderDefault", dtkMeshReaderDefaultCreator);
    }
    void setVerboseLoading(bool b)
    {
        manager().setVerboseLoading(b);
    }

    void setAutoLoading(bool auto_load)
    {
        manager().setAutoLoading(auto_load);
    }

    void uninitialize(void)
    {
        manager().uninitialize();
    }

}

//
// dtkDiscreteGeometry.cpp ends here
