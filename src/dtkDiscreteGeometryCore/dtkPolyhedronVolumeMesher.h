// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>
#include <QVariant>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkPolyhedronVolumeMesher : public QRunnable
{
public:
    virtual void setMesh(const dtkMesh *mesh) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh *mesh(void) const = 0;

    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkPolyhedronVolumeMesher*);
DTK_DECLARE_PLUGIN(dtkPolyhedronVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkPolyhedronVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkPolyhedronVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkPolyhedronVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT, polyhedronVolumeMesher);
}

//
// dtkPolyhedronVolumeMesher.h ends here
