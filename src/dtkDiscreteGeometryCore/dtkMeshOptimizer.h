// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>
#include <QVariant>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshOptimizer : public QRunnable
{
public:
    virtual void setMesh(dtkMesh *mesh) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh *mesh(void) const = 0;
    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkMeshOptimizer*);
DTK_DECLARE_PLUGIN(        dtkMeshOptimizer, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshOptimizer, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshOptimizer, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshOptimizer, DTKDISCRETEGEOMETRYCORE_EXPORT, meshOptimizer);
}

//
// dtkMeshOptimizer.h ends here
