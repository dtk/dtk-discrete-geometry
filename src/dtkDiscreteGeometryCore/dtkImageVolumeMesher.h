// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QtCore>

#include <dtkCore>

class dtkImage;
class dtkMesh;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkImageVolumeMesher : public QRunnable
{
public:
    virtual void setImage(const dtkImage *image) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh *mesh(void) const = 0;
    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkImageVolumeMesher *);
DTK_DECLARE_PLUGIN(        dtkImageVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkImageVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkImageVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkImageVolumeMesher, DTKDISCRETEGEOMETRYCORE_EXPORT, imageVolumeMesher);
}

//
// dtkImageVolumeMesher.h ends here
