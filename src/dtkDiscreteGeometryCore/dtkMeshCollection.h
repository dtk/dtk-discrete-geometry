// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkDiscreteGeometryCoreExport.h>

// /////////////////////////////////////////////////////////////////
// dtkMeshCollection
// /////////////////////////////////////////////////////////////////

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshCollection
{

public:
    explicit dtkMeshCollection(void) {}

public:
    virtual ~dtkMeshCollection(void) {}

public:
    inline dtkMeshCollection& operator=(const dtkMeshCollection& other)
    {
        if(m_meshes != other.m_meshes) {
            if(m_meshes.size() != 0) {
                m_meshes.clear();
            }
            if(other.m_meshes.size() != 0) {
                for (auto it = other.m_meshes.begin(); it!=other.m_meshes.end(); ++it) {
                    m_meshes[it.key()]=it.value();
                }
            }
        }
        return *this;
    }

    inline dtkMesh *& operator[](unsigned int key) {
        return m_meshes[key];
    }

    inline int remove(unsigned int key) {
        return m_meshes.remove(key);
    }

    inline QList<dtkMesh*>* values() const {
        QList<dtkMesh*>* list_ptr=new QList<dtkMesh*>(m_meshes.values());
        return list_ptr;
    }

protected:
    QMap<unsigned int, dtkMesh*> m_meshes;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshCollection*)
DTK_DECLARE_PLUGIN(dtkMeshCollection, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshCollection, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshCollection, DTKDISCRETEGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshCollection, DTKDISCRETEGEOMETRYCORE_EXPORT, meshCollection);
}

//
// dtkMeshCollection.h ends here
