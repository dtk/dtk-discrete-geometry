// Version: $Id: 5b9687a90d4f59286ee07a7bbdee7e5499f05cc9 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryCoreSettings.h"

dtkDiscreteGeometryCoreSettings::dtkDiscreteGeometryCoreSettings(void) : QSettings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-discrete-geometry-core")
{
    this->beginGroup("discrete-geometry-core");

    if(!this->allKeys().contains("plugins"))
        this->setValue("plugins", QString());

    this->sync();
    this->endGroup();
}

dtkDiscreteGeometryCoreSettings::~dtkDiscreteGeometryCoreSettings(void)
{

}

//
// dtkDiscreteGeometrySettings.cpp ends here
