// Version: $Id:  $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include<dtkDiscreteGeometryCoreExport.h>

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// Layer methods declarations
// ///////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {

    DTKDISCRETEGEOMETRYCORE_EXPORT dtkCoreLayerManager& manager(void);

    DTKDISCRETEGEOMETRYCORE_EXPORT void initialize(const QString& path = QString());
    DTKDISCRETEGEOMETRYCORE_EXPORT void setVerboseLoading(bool b);
    DTKDISCRETEGEOMETRYCORE_EXPORT void setAutoLoading(bool auto_loading);
    DTKDISCRETEGEOMETRYCORE_EXPORT void uninitialize(void);

}

//
// dtkDiscreteGeometry.h ends here
