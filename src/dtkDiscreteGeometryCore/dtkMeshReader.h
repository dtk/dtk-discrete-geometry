// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshReader : public QRunnable
{
public:
    virtual void setMeshFile(const QString&) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh *mesh(void) const = 0;

    virtual const QStringList& extensions(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshReader*);
DTK_DECLARE_PLUGIN(dtkMeshReader, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshReader, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshReader, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshReader, DTKDISCRETEGEOMETRYCORE_EXPORT, meshReader);
}

//
// dtkMeshReader.h ends here
