// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkBrepVolumeMesher.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkBrepVolumeMesher
  \brief The dtkBrepVolumeMesher class is an abstraction defining a volumic mesher which discretizes a domain defined as a closed boundary representation of trimmed nurbs surfaces into a polyelement mesh.

  \since 1.0.0
  \ingroup meshers
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkBrepVolumeMesher::setBrep(const dtkBrep *brep) = 0
*/

/*!
  \fn virtual void dtkBrepVolumeMesher::run(void) = 0
*/

/*!
  \fn virtual dtkMesh *dtkBrepVolumeMesher::mesh(void) const = 0
*/

/*!
  \fn virtual QVariantMap dtkBrepVolumeMesher::statistics(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkBrepVolumeMesher, brepVolumeMesher, dtkDiscreteGeometryCore);
}

//
// dtkBrepVolumemesher.cpp ends here
