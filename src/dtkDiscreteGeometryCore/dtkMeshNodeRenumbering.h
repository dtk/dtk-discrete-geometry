// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>

#include <dtkCore>

class dtkMesh;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshNodeRenumbering : public QRunnable
{
public:
    virtual void setMesh(dtkMesh *) = 0;

public:
    virtual dtkMesh *reorderedMesh(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshNodeRenumbering*);
DTK_DECLARE_PLUGIN(        dtkMeshNodeRenumbering, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshNodeRenumbering, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshNodeRenumbering, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshNodeRenumbering, DTKDISCRETEGEOMETRYCORE_EXPORT, meshNodeRenumbering);
}

//
// dtkMeshNodeRenumbering.h ends here
