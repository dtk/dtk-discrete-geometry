// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>

#include <dtkCore>

#include <cstddef>

class dtkMeshPartitionMap;
class dtkMesh;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshPartitioner : public QRunnable
{
public:
    virtual void setMesh(const dtkMesh *mesh) = 0;
    virtual void setBalance(double balance) = 0;
    virtual void setPartitionCount(std::size_t partition_count) = 0;
    virtual void setMeshPartitionMap(dtkMeshPartitionMap *map) = 0;

    virtual const dtkMeshPartitionMap *meshPartitionMap(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkMeshPartitioner*);
DTK_DECLARE_PLUGIN(        dtkMeshPartitioner, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshPartitioner, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshPartitioner, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshPartitioner, DTKDISCRETEGEOMETRYCORE_EXPORT, meshPartitioner);
}

//
// dtkMeshPartitioner.h ends here
