// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMesher.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMesher, mesher, dtkDiscreteGeometryCore);
}

//
// dtkMesher.cpp ends here
