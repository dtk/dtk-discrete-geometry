// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QVariant>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshesComparator : public QRunnable
{
public:
    virtual void setMeshes(const dtkMesh* mesh_a, const dtkMesh *mesh_b) = 0;

    virtual void run(void) = 0;

    virtual QVariantMap statistics(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshesComparator*);
DTK_DECLARE_PLUGIN(dtkMeshesComparator, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshesComparator, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshesComparator, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshesComparator, DTKDISCRETEGEOMETRYCORE_EXPORT, meshesComparator);
}

//
// dtkMeshesComparator.h ends here
