// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshWriter : public QRunnable
{
public:
    virtual void setInputDtkMesh(const dtkMesh* const) = 0;
    virtual void setOutputMeshFile(const QString&) = 0;

    virtual void run(void) = 0;

    virtual QString outputMeshFile(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshWriter*);
DTK_DECLARE_PLUGIN(dtkMeshWriter, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshWriter, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshWriter, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshWriter, DTKDISCRETEGEOMETRYCORE_EXPORT, meshWriter);
}

//
// dtkMeshWriter.h ends here
