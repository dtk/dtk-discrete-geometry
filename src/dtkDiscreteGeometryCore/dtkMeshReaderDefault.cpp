// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshReaderDefault.h"

#include "dtkMesh"
#include "dtkMeshData"
#include "dtkMeshDataDefault.h"

// /////////////////////////////////////////////////////////////////
// dtkMeshReaderDefaultPrivate
// /////////////////////////////////////////////////////////////////

class dtkMeshReaderDefaultPrivate
{
public:
    QString in_mesh_file;
    dtkMesh* dtk_mesh;

    QStringList extensions;
};

// ///////////////////////////////////////////////////////////////////
// dtkMeshReaderDefault implementation
// ///////////////////////////////////////////////////////////////////

dtkMeshReaderDefault::dtkMeshReaderDefault(void) : dtkMeshReader(), d(new dtkMeshReaderDefaultPrivate)
{
    d->dtk_mesh = nullptr;
    d->extensions << "off" << "OFF";
}

dtkMeshReaderDefault::~dtkMeshReaderDefault(void)
{
    delete d;
    d = nullptr;
}

void dtkMeshReaderDefault::setMeshFile(const QString& file)
{
    d->in_mesh_file = file;
}

void dtkMeshReaderDefault::run(void)
{
    //Generate an empty mesh_data
    dtkMeshDataDefault* mesh_data = new dtkMeshDataDefault();

    Q_ASSERT(mesh_data != nullptr);

    mesh_data->setGeometricalDimension(3);//the surface will be embedded in an 3D Euclidian space
    mesh_data->setTopologicalDimension(2);//vertex, edges, facets

    //Parse file name to recover extension
    //Test extesion among all extensions which can generate a polyhedral surface
    //For the moment only off is available
    if (QFileInfo(d->in_mesh_file).suffix() != "off" && QFileInfo(d->in_mesh_file).suffix() != "off"){
        dtkError() << Q_FUNC_INFO << "The file doesn't match the possible file formats";
        return;
    }

    QFile file(d->in_mesh_file);
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkError() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }
    QString line = in->readLine().trimmed();
    QRegExp re = QRegExp("\\s+");

    //Run tests on the file to check its validity TODO more
    if ( line != "OFF"){
        dtkError() << Q_FUNC_INFO << ".off file not consistent with OFF format";
        return;
    }
    while (true && !in->atEnd()) {
        line = in->readLine().trimmed();
        if (!line.startsWith("#"))
            break;
    }

    QStringList vals = line.split(re);
    unsigned int vertices_nb = vals[0].toInt(), faces_nb = vals[1].toInt(), edges_nb = vals[2].toInt();
    // Set points and number of points

    mesh_data->setPointsCount(vertices_nb);

    unsigned int vertex_id = 0;
    while(vertex_id < vertices_nb) {
        line = in->readLine().trimmed();
        if (line.startsWith("#"))
            continue;
        std::istringstream lin(line.toStdString());
        double x, y, z;
        lin >> x >> y >> z;       //now read the whitespace-separated floats
        double coords[3]={x,y,z};//x,y,z points
        mesh_data->setPoint(vertex_id, coords);
        vertex_id ++;
    }

    // Initialize arrays of types and points of cells
    dtkArray<dtkMeshData::CellType> cells_type;
    cells_type.reserve(vertices_nb+faces_nb+edges_nb);
    dtkArray<dtkMeshData::IdxType> cells_points;
    //read the file once first to check the number of points first ?
    cells_points.reserve(vertices_nb*nb_points_per_cell_type[dtkMeshData::Vertex]
            +faces_nb*nb_points_per_cell_type[dtkMeshData::Quad]+
            +edges_nb*nb_points_per_cell_type[dtkMeshData::Line]); // tolerates up to quads TODO

    // Recovers vertices2
    for(unsigned int vertex_id = 0; vertex_id < vertices_nb; ++vertex_id) {
        cells_type.append(dtkMeshData::Vertex);
        // dtk cells_points array format = [... nb_pts pt_id pt_id ...]
        cells_points.append(nb_points_per_cell_type[dtkMeshData::Vertex]);
        cells_points.append(vertex_id);
    }
    //Edges are not specified in this format

    // Recovers triangular faces //TODO make it work for any type of face -> need to parse the line better
    unsigned int face_id = 0;
    while(face_id < faces_nb) {
        line = in->readLine().trimmed();
        if (line.startsWith("#"))
            continue;

        std::istringstream lin(line.toStdString());
        unsigned int ids_nb, first, second, third;
        lin >> ids_nb >> first >> second >> third;
        cells_type.append(dtkMeshData::Triangle);
        // dtk cells_points array format = [... nb_pts pt_id pt_id ...]
        cells_points.append(ids_nb);
        cells_points.append(first);
        cells_points.append(second);
        cells_points.append(third);
        face_id ++;
    }

    // Set cell types, cell points and number of cells by dimension
    mesh_data->setCellsCount(vertices_nb, 0);
    mesh_data->setCellsCount(edges_nb, 1);//Edges are not specified in this format or if specified not explicitely given
    mesh_data->setCellsCount(faces_nb, 2);
    mesh_data->setCellsType(cells_type.data());
    mesh_data->setCellsPoints(cells_points.constData(), cells_points.size());

    // Initialize dtk mesh
    d->dtk_mesh = new dtkMesh(mesh_data);
}

dtkMesh* dtkMeshReaderDefault::mesh(void) const
{
    return d->dtk_mesh;
}

const QStringList& dtkMeshReaderDefault::extensions(void) const
{
    return d->extensions;
}

//
// dtkMeshReaderDefault.cpp ends here
