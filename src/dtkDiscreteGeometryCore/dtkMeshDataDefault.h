/* dtkMeshDataDefault.h ---
 *
 * Version:
 *
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <dtkCore>

#include "dtkMeshData.h"
#include "dtkAttribute.h"

class dtkMeshDataDefaultPrivate;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshDataDefault final : public dtkMeshData
{
public:
    dtkMeshDataDefault(void);
    dtkMeshDataDefault(const dtkMeshDataDefault& mesh_data);
    ~dtkMeshDataDefault(void) final ;

public:
    dtkMeshDataDefault* clone(void) const override;

public:
    int geometricalDimension(void) const override;
    void setGeometricalDimension(int geo_dim) override;
    int topologicalDimension(void) const override;
    void setTopologicalDimension(int topo_dim) override;

public:
    void pointCoordinates(dtkMeshData::IdxType id, double* point_coordinates) const override;
    const double* pointsCoordinates(void) const override;
    dtkMeshData::CntType pointsCount(void) const override;

    void setPoint(dtkMeshData::IdxType point_id, const double* points_coordinates) override;
    void setPointsCount(dtkMeshData::CntType point_count) override;
    void setPoints(const double* points_coordinates, dtkMeshData::CntType points_count = 0) override;

public:
    void cellPoints(dtkMeshData::IdxType cell_id, int &out_nb_points_cell, const dtkMeshData::IdxType*& out_first_point) const override; // out nb points of cell , out_first_point pointer to the first point of the cell
    dtkMeshData::CellType cellType(dtkMeshData::IdxType cell_id) const override;
    dtkMeshData::CntType cellsCount(int geo_dimension = 4) const override;
    const dtkMeshData::IdxType* cellsIdx(dtkMeshData::CntType& out_nb_cells, int geo_dimension = 4) const override;
    const dtkMeshData::IdxType* cellsPoints(dtkMeshData::CntType& out_nb_cells, int geo_dimension = 4) const override;
    const dtkMeshData::CellType* cellsType(int geo_dimension = 4) const override;
    const dtkMeshData::IdxType* cellsTopologyLocation(int geo_dimension = 4) const override;
    unsigned int facet_corner_id(const dtkMeshData::IdxType face_id, const dtkMeshData::IdxType corner_id) const;

    void setCellsCount(dtkMeshData::CntType cells_count, int geo_dimension) override;
    void setCellsType(const dtkMeshData::CellType* cells_type, dtkMeshData::CntType cells_count = 0, dtkMeshData::CntType offset=0) override;
    void setCellsPoints(const dtkMeshData::IdxType* cells_points, dtkMeshData::CntType cells_count = 0) override;

public:
    void addAttribute(const dtkAttribute * attribute) override;
    const dtkAttribute* attribute(const QString& name) const override;
    const dtkAttribute* attributes(void) const override;
    dtkMeshData::CntType attributesCount(void) const override;

private:
    int m_geo_dim;
    int m_topo_dim;

	dtkMeshData::CntType m_nb_points;
    dtkMeshData::CntType m_nb_cells[5]; // 5 : total size, {0,1,2,3} nb of cells od DIM {0,1,2,3}

    dtkArray<double> m_points_coordinates; //3D, size = 3*nb_points
    dtkArray<dtkMeshData::CellType> m_cells_type;
    dtkArray<dtkMeshData::IdxType> m_cells_first_point_idx; //index of first point in m_cell_points per cell
    dtkArray<dtkMeshData::IdxType> m_cells_points;
    dtkArray<dtkAttribute> m_attributes;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkMeshData *dtkMeshDataDefaultCreator(void)
{
    return new dtkMeshDataDefault();
}

//
// dtkMeshDataDefault.h ends here
