// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAttribute.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkAttribute
  \brief dtkAttribute contains the attributes/properties of the nodes and elements of a \l dtkMesh. It is to be used as a Struct.

  \inmodule dtkDiscreteGeometryCore
 */

/*! \fn dtkAttribute::dtkAttribute(void)
   Constructor.
*/

/*! \fn dtkAttribute::dtkAttribute(const int& rank, const QString& name, const unsigned char& kind, const unsigned char& support,const dtkArray<double>& data)
   Constructor which sets the public members of the class, all at one.
*/

/*! \fn dtkAttribute::dtkAttribute(const dtkAttribute& other)
   Copy constructor
*/

/*! \fn dtkAttribute::~dtkAttribute(void)
   Destructor.
*/
