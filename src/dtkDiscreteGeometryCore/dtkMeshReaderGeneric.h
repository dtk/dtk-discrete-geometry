// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include "dtkMeshReader.h"

#include <dtkDiscreteGeometryCoreExport.h>

class dtkMesh;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshReaderGeneric : public dtkMeshReader
{
public:
     dtkMeshReaderGeneric(void);
    ~dtkMeshReaderGeneric(void);

public:
    void  setMeshFile(const QString&) final;

    void run(void) final;

    dtkMesh *mesh(void) const final;

    const QStringList& extensions(void) const final;

private:
    QString m_in_mesh_file;
    dtkMesh* m_dtk_mesh;

    QList<dtkMeshReader*> m_mesh_readers;
    QStringList m_extensions;
};

// ///////////////////////////////////////////////////////////////////

inline dtkMeshReader* dtkMeshReaderGenericCreator(void)
{
    return new dtkMeshReaderGeneric();
}

//
// dtkMeshReaderGeneric.h ends here
