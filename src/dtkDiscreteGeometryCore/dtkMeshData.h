// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once


#include <QtCore>
#include <dtkCore>

#include <dtkDiscreteGeometryCoreExport.h>

class dtkAttribute;

static quint32 nb_points_per_cell_type[25] = {0, 1, 0, 2, 0,
        3, 0, 0, 0, 4,
        4, 0, 8, 6, 0,
        0, 0, 0, 0, 0,
        0, 4, 0, 0, 0
                                             };

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////
class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshData
{
public:
    typedef long long int IdxType; // index type
    typedef long long int CntType; //counter type ( nb points, nb cells, ..)

    enum CellType : unsigned char {
        Vertex =  1,
        Line =  3,
        Quad = 9,
        QuadLine = 21,
        Triangle =  5,
        Tetra = 10,
        Hexahedron = 12,
        Wedge = 13
    }; //type unsigned char

public:
    dtkMeshData(void) {
        ;
    };
    dtkMeshData(const dtkMeshData& mesh_data);
    virtual ~dtkMeshData(void) {
        ;
    };

public:
    virtual dtkMeshData* clone(void) const = 0;

public:

// geometry related
public:
    virtual int geometricalDimension(void) const;
    virtual void setGeometricalDimension(int geo_dim);

    virtual int topologicalDimension(void) const;
    virtual void setTopologicalDimension(int topo_dim);
public:
    virtual void pointCoordinates(IdxType id, double* point_coordinates) const;
    virtual const double* pointsCoordinates(void) const;
    virtual CntType pointsCount(void) const;

    virtual void setPoint(IdxType point_id, const double* points_coordinates);
    virtual void setPointsCount(CntType point_count);
    virtual void setPoints(const double* points_coordinates, CntType points_count = 0);

public:
    virtual void  cellPoints(IdxType cell_id, int &out_nb_points_cell, const IdxType*& out_first_point) const; // out nb points of cell , out_first_point pointer to the first point of the cell
    virtual CellType cellType(IdxType cell_id) const;
    virtual CntType cellsCount(int geo_dimension = 4) const;
    virtual const IdxType* cellsIdx(CntType& out_nb_cells, int geo_dimension = 4) const;
    virtual const IdxType* cellsPoints(CntType& out_nb_points, int geo_dimension = 4) const;
    virtual const CellType* cellsType(int geo_dimension = 4) const;
    virtual const IdxType* cellsTopologyLocation(int geo_dimension = 4) const;
    virtual void typedCellPointsIds(const CellType cell_type, unsigned int cell_id, dtkMeshData::IdxType* out_points_ids) const;

public:
    virtual unsigned int* lower_degree_ele_ids(unsigned int ele_dim, const dtkMeshData::IdxType ele_id) const;

public:
    virtual void setCellsCount(CntType cells_count, int geo_dimension);
    virtual void setCellsType(const CellType* cells_type, CntType cells_count = 0, CntType offset=0 );
    virtual void setCellsPoints(const IdxType* cells_points, CntType size_cells_point = 0);
// topology related
//public:
//    virtual int topologicalDimension(void);

// attributes related
public:
    virtual void addAttribute(const dtkAttribute* attribute);
    virtual const dtkAttribute* attribute(const QString& name) const;
    virtual const dtkAttribute* attributes(void) const;
    virtual CntType attributesCount(void) const;

};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkMeshData*)
DTK_DECLARE_PLUGIN(dtkMeshData, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshData, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshData, DTKDISCRETEGEOMETRYCORE_EXPORT)


// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshData, DTKDISCRETEGEOMETRYCORE_EXPORT, meshData);
}


//
// dtkMeshData.h ends here
