// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshLabeler.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkMeshLabeler
  \brief The dtkMeshLabeler class is an abstraction defining a labeler which set properties onto the mesh elements. See /l dtkAttribute.

  \since 1.0.0
  \ingroup labelers
  \inmodule dtkDiscreteGeometryCore
*/


/*!
  \fn virtual void dtkMeshLabeler::setMesh(dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkMeshLabeler::run(void) = 0
*/

/*!
  \fn virtual dtkMesh *dtkMeshLabeler::mesh(void) const = 0
*/

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshLabeler, meshLabeler, dtkDiscreteGeometryCore);
}

//
// dtkMeshLabeler.cpp ends here
