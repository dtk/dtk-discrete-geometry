// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshOptimizer.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkMeshOptimizer
  \brief The dtkMeshOptimizer class is an abstraction defining a mesh quality enhancer, which can reduce the number of elements, remove bad quality elements and so on...

  \since 1.0.0
  \ingroup optimizers
  \inmodule dtkDiscreteGeometry
*/

/*!
  \fn virtual void dtkMeshOptimizer::setMesh(dtkMesh *mesh) = 0
*/

/*!
  \fn virtual void dtkMeshOptimizer::run(void) = 0
*/

/*!
  \fn virtual dtkMesh *dtkMeshOptimizer::mesh(void) const = 0
*/

/*!
  \fn virtual QVariantMap dtkMeshOptimizer::statistics(void) const = 0
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshOptimizer, meshOptimizer, dtkDiscreteGeometryCore);
}

//
// dtkMeshOptimizer.cpp ends here
