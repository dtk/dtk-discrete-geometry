// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshView.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshView, meshView, dtkDiscreteGeometryCore);
}

//
// dtkMeshView.cpp ends here
