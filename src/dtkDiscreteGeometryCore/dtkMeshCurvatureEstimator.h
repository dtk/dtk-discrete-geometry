// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkMesh;
class dtkMeshCollection;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshCurvatureEstimator : public QRunnable
{
public:
    virtual void  setMesh(dtkMesh *mesh) = 0;

    virtual void run(void) = 0;

    virtual dtkMesh* mesh(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshCurvatureEstimator*);
DTK_DECLARE_PLUGIN(dtkMeshCurvatureEstimator, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshCurvatureEstimator, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshCurvatureEstimator, DTKDISCRETEGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshCurvatureEstimator, DTKDISCRETEGEOMETRYCORE_EXPORT, meshCurvatureEstimator);
}

//
// dtkMeshCurvatureEstimator.h ends here
