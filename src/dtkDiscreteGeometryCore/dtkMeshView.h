/* dtkMeshView.h ---
 *
 * Version:
 *
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */



#pragma once

#include "dtkDiscreteGeometryCore.h"

#include <QtCore>
#include <QtWidgets/QWidget>

#include <dtkCore>

#include <dtkDiscreteGeometryCoreExport.h>

class dtkMesh;

static QStringList empty_list;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshView : public QObject
{
    Q_OBJECT

public:
    explicit dtkMeshView(void) : m_mesh(nullptr) {
        ;
    }
    explicit dtkMeshView(dtkMesh *mesh) :m_mesh(mesh) {
        ;
    }
    ~dtkMeshView(void) {
        m_mesh=nullptr;
    };


public:
    virtual void *camera(void) {
        return nullptr;
    }
    virtual void *interactor(void) {
        return nullptr;
    }
    dtkMesh *mesh(void) {
        return m_mesh;
    }
    virtual void *renderer(void) {
        return nullptr;
    }
    virtual void *window(void) {
        return nullptr;
    }

public:
    virtual void setMesh(dtkMesh * mesh) {
        m_mesh = mesh;
    }
    virtual void setParameters(const QStringList& /*list*/) {
        ;
    }
    virtual void setWindow(void */*win*/) {
        ;
    }

public slots:
    virtual QWidget *widget(void) {
        return nullptr;
    }
    virtual void update(const QStringList& /*l*/=empty_list) {
        ;
    }

protected:
    dtkMesh *m_mesh;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshView*)
DTK_DECLARE_PLUGIN(dtkMeshView, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshView, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshView, DTKDISCRETEGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshView, DTKDISCRETEGEOMETRYCORE_EXPORT, meshView);
}

//
// dtkMeshView.h ends here
