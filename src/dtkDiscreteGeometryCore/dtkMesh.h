// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QtCore>

#include <dtkCore>

#include "dtkMeshData.h"

#include <iostream>

// /////////////////////////////////////////////////////////////////
// dtkMesh
// /////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMesh
{

public:
    explicit dtkMesh(void) : m_data(nullptr) {
        ;
    };
    explicit dtkMesh(dtkMeshData *engine) : m_data(engine) {
        ;
    };
    dtkMesh(const dtkMesh& other) :m_data(other.m_data->clone()) {
    std::cout << "COPY" << std::endl;
    };

public:
    virtual ~dtkMesh(void) {
        if (m_data)
            delete m_data;
        m_data = NULL;
    };

public:
    dtkMesh& operator=(const dtkMesh& other)
    {
        if(m_data != other.m_data) {
            if(m_data != nullptr) {
                delete m_data;
            }
            if(other.m_data != nullptr) {
                m_data = other.m_data->clone();
            } else {
                m_data = nullptr;
            }
        }
        return *this;
    }

public:
    const dtkMeshData* data(void) const {
        return m_data;
    };
    dtkMeshData *data(void)       {
        return m_data;
    };
    void setData(dtkMeshData* data) {
        m_data = data;
    };

protected:
    dtkMeshData *m_data;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMesh*)
DTK_DECLARE_PLUGIN(dtkMesh, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkMesh, DTKDISCRETEGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkMesh, DTKDISCRETEGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMesh, DTKDISCRETEGEOMETRYCORE_EXPORT, mesh)
}

//
// dtkMesh.h ends here
