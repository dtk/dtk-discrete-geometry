// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkImageVolumeMesher.h"

#include "dtkDiscreteGeometryCore.h"

/*!
  \class dtkImageVolumeMesher
  \brief The dtkImageVolumeMesher class is an abstraction defining a volumic mesher which discretizes a domain defined by a 3DImage into a polyelement mesh.

  \since 1.0.0
  \ingroup meshers
  \inmodule dtkDiscreteGeometryCore
*/

/*!
  \fn virtual void dtkImageVolumeMesher::setImage(const dtkImage *image) = 0;
*/

/*!
  \fn virtual void dtkImageVolumeMesher::run(void) = 0;
*/

/*!
  \fn virtual dtkMesh*dtkImageVolumeMesher::mesh(void) const = 0;
*/

/*!
  \fn virtual QVariantMap dtkImageVolumeMesher::statistics(void) const = 0;
*/


// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometryCore layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkImageVolumeMesher, imageVolumeMesher, dtkDiscreteGeometryCore);
}

//
// dtkImageVolumeMesher.cpp ends here
