// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshPartitionMap.h"

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// dtkMeshPartitionMap implementation
// ///////////////////////////////////////////////////////////////////

dtkMeshPartitionMap::~dtkMeshPartitionMap(void)
{
    m_partition_count = 0;
}

bool dtkMeshPartitionMap::read(const QString& filename)
{
    QFile file(filename);

    if (filename.isEmpty() || (!file.exists())) {
        dtkError() << "dtkMeshPartitionMap::read() : input file is empty/does not exist" << filename << "Current dir is" << QDir::currentPath();
        return false;
    }

    QIODevice::OpenMode mode = QIODevice::ReadOnly | QIODevice::Text;

    if (!file.open(mode)) {
        dtkError() << "dtkMeshPartitionMap::read() : input file" << filename << "cannot be opened.";
        return false;
    }

    QRegExp re = QRegExp("\\s+");
    QString line;

    // Skip comments
    do {
        line = file.readLine();
    } while (line.isEmpty() || line.startsWith("%") || line.startsWith("#"));

    // Reads header
    QStringList header = line.split(re);

    m_partition_count         = header[0].toULong();
    std::size_t element_count = header[1].toULong();
    std::size_t vertex_count  = header[2].toULong();

    m_elmt_parts.reserve(element_count);
    m_vert_parts.reserve(vertex_count);

    // Reads partition owner for each element
    for (std::size_t i = 0; i < element_count; ++i) {
        do {
            line = file.readLine().trimmed();
        } while (line.isEmpty() || line.startsWith("%") || line.startsWith("#"));

        m_elmt_parts.push_back(line.toLong());
    }

    // Reads partition owner for each vertex
    for (std::size_t i = 0; i < vertex_count; ++i) {
        do {
            line = file.readLine().trimmed();
        } while (line.isEmpty() || line.startsWith("%") || line.startsWith("#"));

        m_vert_parts.push_back(line.toLong());
    }

    file.close();

    return true;
}

bool dtkMeshPartitionMap::write(const QString& filename) const
{
    if (filename.isEmpty()) {
        dtkError() << "dtkMeshPartitionMap::write() : output filename is empty. Nothing is done.";
        return false;
    }

    QFile file(filename);

    QIODevice::OpenMode mode = QIODevice::WriteOnly | QIODevice::Text;

    if (!file.open(mode)) {
        dtkError() << "dtkMeshPartitionMap::write() : output file" << filename << "cannot be opened.";
        return false;
    }

    QString header = QString("%1 %2 %3\n\n")
        .arg(QString::number(m_partition_count))
        .arg(QString::number(m_elmt_parts.size()))
        .arg(QString::number(m_vert_parts.size()));

    file.write(qPrintable(header), header.size());

    QString line;

    for (long int id : m_elmt_parts) {
        line = QString::number(id) + "\n";
        file.write(qPrintable(line), line.size());
    }
    file.write("\n", 1);

    for (long int id : m_vert_parts) {
        line = QString::number(id) + "\n";
        file.write(qPrintable(line), line.size());
    }

    file.close();

    return true;
}

//
// dtkMeshPartitionMap.cpp ends here
