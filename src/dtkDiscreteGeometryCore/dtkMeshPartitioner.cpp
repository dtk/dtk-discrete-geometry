// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshPartitioner.h"

#include "dtkDiscreteGeometryCore.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscreteGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DEFINE_CONCEPT(dtkMeshPartitioner, meshPartitioner, dtkDiscreteGeometryCore);
}

//
// dtkMeshPartitioner.cpp ends here
