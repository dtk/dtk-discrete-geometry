// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkMesh;
class dtkMeshCollection;

class DTKDISCRETEGEOMETRYCORE_EXPORT dtkMeshSurfacesExtractor : public QRunnable
{
public:
    virtual void setMesh(const dtkMesh *mesh) = 0;

    virtual void run(void) = 0;

    virtual dtkMeshCollection *meshCollection(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkMeshSurfacesExtractor*);
DTK_DECLARE_PLUGIN(dtkMeshSurfacesExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkMeshSurfacesExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkMeshSurfacesExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT);


// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkDiscreteGeometryCore {
    DTK_DECLARE_CONCEPT(dtkMeshSurfacesExtractor, DTKDISCRETEGEOMETRYCORE_EXPORT, meshSurfacesExtractor);
}

//
// dtkMeshSurfacesExtractor.h ends here
