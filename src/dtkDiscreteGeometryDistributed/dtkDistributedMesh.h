// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryDistributedExport.h>

#include <vector>

#include <dtkDistributed>

#include "dtkDistributedMeshConnectivity.h"

class dtkMesh;
class dtkMeshPartitionMap;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYDISTRIBUTED_EXPORT dtkDistributedMesh
{
public:
     dtkDistributedMesh(void) = default;
     dtkDistributedMesh(const dtkMesh *mesh, const dtkMeshPartitionMap *map);
     //dtkDistributedMesh(const dtkDistributedMesh& o);
     ~dtkDistributedMesh(void);

public:
     //dtkDistributedMesh& operator = (const dtkDistributedMesh& o);

public:
    std::size_t topologicalDimension(void) const { return m_entity_counts.size() - 1; }
    std::size_t geometricalDimension(void) const { return m_geom_dim; }

public:
    std::size_t vertexCount(void) const { if (m_entity_counts.empty()) return 0; return m_entity_counts[0]; }
    std::size_t   edgeCount(void) const { if (m_entity_counts.empty()) return 0; return m_entity_counts[1]; }
    std::size_t   faceCount(void) const { if (m_entity_counts.empty()) return 0; return m_entity_counts[2]; }
    std::size_t   cellCount(void) const { if (m_entity_counts.empty()) return 0; return m_entity_counts[m_entity_counts.size() - 1]; }

    std::size_t entityCount(std::size_t dim) const { if (m_entity_counts.empty()) return 0; return m_entity_counts[dim]; }

public:
    const dtkDistributedArray<double> *coordinates(void) const { return m_coords; }
          dtkDistributedArray<double> *coordinates(void)       { return m_coords; }

    void setCoordinates(dtkDistributedArray<double> *coordinates) { if (m_coords) delete m_coords; m_coords = coordinates; }

    const dtkDistributedArray<long long int> *new_connections_new_vid(void) const { return m_new_connections_new_vid ; }
          dtkDistributedArray<long long int> *new_connections_new_vid(void)       { return m_new_connections_new_vid; }

public:
    const dtkDistributedMeshConnectivity& connectivity(std::size_t entities_dim, std::size_t connections_dim) const;
          dtkDistributedMeshConnectivity& connectivity(std::size_t entities_dim, std::size_t connections_dim);

    //const dtkDistributedArray<std::size_t> *cellConnectivities(void) const;

public:
    void buildConnectivities(void);

private:
    dtkDistributedCommunicator *m_comm;

private:
    long int m_geom_dim;

    dtkDistributedArray<double> *m_coords;
    dtkDistributedArray<long long int> *m_new_connections_new_vid;

private:
    std::vector<long int> m_entity_counts; // Number of mesh entities for each topological dimension

    std::vector< std::vector<dtkDistributedMeshConnectivity> > m_connectivities; // Connectivity for pairs of topological dimensions


};

//
// dtkDistributedMesh.h ends here
