// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistributedMeshConnectivity.h"

#include <sstream>


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(void) : m_entities_dim(23UL), m_connections_dim(23UL), m_graph(nullptr)
{

}

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim) : m_entities_dim(entities_dim), m_connections_dim(connections_dim), m_graph(nullptr)
{

}

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, std::size_t entities_count) : m_entities_dim(entities_dim), m_connections_dim(connections_dim), m_graph(new dtkDistributedGraphTopology(entities_count))
{

}

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, dtkDistributedGraphTopology *graph) : m_entities_dim(entities_dim), m_connections_dim(connections_dim), m_graph(graph)
{
    Q_ASSERT_X(m_graph, "", "dtkDistributedMeshConnectivity::Ctor(): Input graph must be valid.");

    m_graph->ref();
}

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(const dtkDistributedMeshConnectivity& o) : m_entities_dim(o.m_entities_dim), m_connections_dim(o.m_connections_dim), m_graph(o.m_graph)
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::CopyCtor(): Dimensions of entities and connections must be valid in source object. ");
    Q_ASSERT_X(m_graph, "", "dtkDistributedMeshConnectivity::CopyCtor(): Graph must be valid in source object.");

    m_graph->ref();
}

dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(dtkDistributedMeshConnectivity&& o) : m_entities_dim(std::move(o.m_entities_dim)), m_connections_dim(std::move(o.m_connections_dim)), m_graph(std::move(o.m_graph))
{
    o.m_graph = nullptr;
    o.clear();
}

dtkDistributedMeshConnectivity::~dtkDistributedMeshConnectivity(void)
{
    this->clear();
}

dtkDistributedMeshConnectivity& dtkDistributedMeshConnectivity::operator = (const dtkDistributedMeshConnectivity& o)
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::operator=(): Dimensions of entities and connections must be valid in source object. ");
    Q_ASSERT_X(o.m_graph, "", "dtkDistributedMeshConnectivity::operato=(): Graph must be valid in source object.");

    if (this != &o) {
        this->clear();
        m_entities_dim = o.m_entities_dim;
        m_connections_dim = o.m_connections_dim;
        m_graph = o.m_graph;
        m_graph->ref();
    }
    return *this;
}

dtkDistributedMeshConnectivity& dtkDistributedMeshConnectivity::operator = (dtkDistributedMeshConnectivity&& o)
{
    this->clear();
    m_entities_dim = std::move(o.m_entities_dim);
    m_connections_dim = std::move(o.m_connections_dim);
    m_graph = std::move(o.m_graph);
    o.m_graph = nullptr;

    return *this;
}

std::size_t dtkDistributedMeshConnectivity::entitiesDimension(void) const
{
    return m_entities_dim;
}

std::size_t dtkDistributedMeshConnectivity::connectionsDimension(void) const
{
    return m_connections_dim;
}

void dtkDistributedMeshConnectivity::setEntitiesDimension(std::size_t dim)
{
    m_entities_dim = dim;
}

void dtkDistributedMeshConnectivity::setConnectionsDimension(std::size_t dim)
{
    m_connections_dim = dim;
}

const dtkDistributedGraphTopology *dtkDistributedMeshConnectivity::graph(void) const
{
    return m_graph;
}

dtkDistributedGraphTopology *dtkDistributedMeshConnectivity::graph(void)
{
    return m_graph;
}

void dtkDistributedMeshConnectivity::setGraph(dtkDistributedGraphTopology *graph)
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::setGraph(): Dimensions of entities and connections must have been previously set.");
    Q_ASSERT_X(graph, "", "dtkDistributedMeshConnectivity::setGraph(): Input Graph must be valid.");

    this->clear();
    m_graph = graph;
    m_graph->ref();
}

void dtkDistributedMeshConnectivity::clear(void)
{
    m_entities_dim = m_connections_dim = 23UL;

    if (m_graph) {
        if (m_graph->isAssembled()) {
            m_graph->unlock();
        }

        if (!m_graph->deref()) {
            delete m_graph;
        }
        m_graph = nullptr;
    }
}

bool dtkDistributedMeshConnectivity::empty(void) const
{
    if (!m_graph) {
        return true;
    }

    return m_graph->empty();
}

std::size_t dtkDistributedMeshConnectivity::size(void) const
{
    if (!m_graph) {
        return 0UL;
    }

    return m_graph->edgeCount();
}

std::size_t dtkDistributedMeshConnectivity::size(std::size_t entity_id) const
{
    if (!m_graph) {
        return 0UL;
    }

    return m_graph->neighbourCount(entity_id);
}

void dtkDistributedMeshConnectivity::connect(std::size_t entity_id, std::size_t connected_entity_id)
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::connect(): Dimensions of entities and connections must have been previously set.");
    Q_ASSERT_X(m_graph, "", "dtkDistributedMeshConnectivity::connect(): Graph must have been previously created.");

    m_graph->addEdge(entity_id, connected_entity_id);
}

typename dtkDistributedMeshConnectivity::Connections dtkDistributedMeshConnectivity::operator () (std::size_t entity_id) const
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::operator()(entity_id): Dimensions of entities and connections must have been previously set.");
    Q_ASSERT_X(m_graph, "", "dtkDistributedMeshConnectivity::operator()(entity_id): Graph must have been previously created.");

    return (*m_graph)[entity_id];
}

void dtkDistributedMeshConnectivity::setAssembleFlag(bool is_assembled)
{
    if (!m_graph) {
        return;
    }
    m_graph->setAssembleFlag(is_assembled);
}

bool dtkDistributedMeshConnectivity::assemble(void)
{
    Q_ASSERT_X((m_entities_dim != 23UL && m_connections_dim != 23UL), "", "dtkDistributedMeshConnectivity::assemble(): Dimensions of entities and connections must have been previously set.");
    Q_ASSERT_X(m_graph, "", "dtkDistributedMeshConnectivity::assemble(): Graph must have been previously created.");

    if (!m_graph->isAssembled()) {
        m_graph->assemble();
        return true;
    }

    return false;
}

std::string dtkDistributedMeshConnectivity::toString(bool verbose) const
{
    std::stringstream sstr;

    if (!verbose) {
        sstr << "dtkDistributedMeshConnectivity between entities of dimension "
             << this->m_entities_dim
             << " and entities of dimension "
             << this->m_connections_dim << "."
             << std::endl;
        sstr << "Number of connections: " << this->size() << std::endl;

    } else {
        sstr << this->toString(false) << std::endl;
        // To do !
    }

    return sstr.str();
}

dtkDistributedMeshConnectivityEntity dtkDistributedMeshConnectivity::begin(void) const
{
    return dtkDistributedMeshConnectivityEntity(m_graph->beginVertex());
}

dtkDistributedMeshConnectivityEntity dtkDistributedMeshConnectivity::end(void) const
{
    return dtkDistributedMeshConnectivityEntity(m_graph->endVertex());
}

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& out, const dtkDistributedMeshConnectivity& mc)
{
    out << mc.toString(false);

    return out;
}

// ///////////////////////////////////////////////////////////////////
// Documentation
// ///////////////////////////////////////////////////////////////////

/*!
  \class dtkDistributedMeshConnectivity
  \inmodule dtkDiscreteGeometry
  \brief The dtkDistributedMeshConnectivity class enables to store
  connections between mesh entities of given topological dimensions.

  The number of connections for each entity can be the same (e.g. when
  considering connectivity between triangles and vertices) or can vary
  from an entity to another (e.g. connectivity between vertices and
  triangles).
*/

/*! \typedef dtkDistributedMeshConnectivity::Connections

  Synonym of \c {dtkDistributedGraphTopology::Neighbours}.

 */

/*! \fn dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim)

  Constructs an object that will connect entities of topological
  dimension \a entities_dim to entities of topological dimension \a
  connections_dim.

  \sa setEntitiesDimension, setConnectionsDimension

*/

/*! \fn dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, std::size_t entities_count)

  Constructs an object that will connect entities of topological
  dimension \a entities_dim to entities of topological dimension \a
  connections_dim. The number of entities to connect is \a
  entities_count.

*/

/*! \fn dtkDistributedMeshConnectivity::dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, dtkDistributedGraphTopology *graph)

  Constructs an object that will connect entities of topological
  dimension \a entities_dim to entities of topological dimension \a
  connections_dim and with an intern graph given by \a graph.

  \sa setGraph
*/

/*! \fn virtual dtkDistributedMeshConnectivity::~dtkDistributedMeshConnectivity(void)

  Destroys the object.

*/

/*! \fn std::size_t dtkDistributedMeshConnectivity::entitiesDimension(void) const

  Returns the topological dimension of the entities.

  \sa setEntitiesDimension, connectionsDimension

*/

/*! \fn std::size_t dtkDistributedMeshConnectivity::connectionsDimension(void) const

  Returns the topological dimension of the connected entities.

  \sa setConnectionsDimension, entitiesDimension

*/

/*! \fn void dtkDistributedMeshConnectivity::setEntitiesDimension(std::size_t dim)

  Sets \a dim as the topological dimension of the entities.

  \sa entitiesDimension, setConnectionsDimension

*/

/*! \fn void dtkDistributedMeshConnectivity::setConnectionsDimension(std::size_t dim)

  Sets \a dim as the topological dimension of the connected entities.

  \sa connectionsDimension, setEntitiesDimension

*/

/*! \fn dtkDistributedGraphTopology *dtkDistributedMeshConnectivity::graph(void)

  Returns the underlying graph.

  \sa setGraph

*/

/*! \fn const dtkDistributedGraphTopology *dtkDistributedMeshConnectivity::graph(void) const

  Returns a const pointer to the underlying graph.

  \sa setGraph

*/

/*! \fn void dtkDistributedMeshConnectivity::setGraph(dtkDistributedGraphTopology *graph)

  Sets the intern graph to \a graph.

  \sa graph
*/

/*! \fn void dtkDistributedMeshConnectivity::clear(void)

  Removes all the connections, resets the topological dimensions,
  deletes the graph when it is not shared.

*/

/*! \fn bool dtkDistributedMeshConnectivity::empty(void) const

  Returns \c true when there is no connections.
*/

/*! \fn std::size_t dtkDistributedMeshConnectivity::size(void) const

  Returns the number of connections.
*/

/*! \fn std::size_t dtkDistributedMeshConnectivity::size(std::size_t entity_id) const

  Returns the number of connections for the entity given by its id \a
  entity_id.
*/

/*! \fn void dtkDistributedMeshConnectivity::connect(std::size_t entity_id, std::size_t connected_entity_id)

  Sets the connections between entity of id \a entity_id and entity of
  id \a connected_entity_id.
*/

/*! \fn Connections dtkDistributedMeshConnectivity::operator () (std::size_t entity_id) const

  Returns the entities connected to the entity of id \a entity_id.
*/

/*! \fn bool dtkDistributedMeshConnectivity::assemble(void)

  Assembles the underlying graph to sets distributed data structures.

  Returns true when assembly process occured successfully.
*/

/*! \fn std::string dtkDistributedMeshConnectivity::toString(bool verbose) const

  Returns string containing more or less information about the object
  according to \a verbose mode.
*/


//
// dtkDistributedMeshConnectivity.cpp ends here
