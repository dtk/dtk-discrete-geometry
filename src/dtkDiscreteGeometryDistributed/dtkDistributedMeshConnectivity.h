// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryDistributedExport.h>

#include <iostream>
#include <iterator>

#include <dtkCore>
#if dtk_VERSION_MAJOR == 1
#  include <dtkDistributedGraphTopology>
#else // dtk-2.x or later
#  include <dtkDistributedData/dtkDistributedGraphTopology>
#endif
// ///////////////////////////////////////////////////////////////////
// dtkDistributedMeshConnectivityEntity
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYDISTRIBUTED_EXPORT dtkDistributedMeshConnectivityEntity
{
    dtkDistributedGraphTopologyVertex m_v;

public:
    typedef std::random_access_iterator_tag  iterator_category;
    typedef std::size_t                      value_type;
    typedef std::ptrdiff_t                   difference_type;
    typedef value_type                      *pointer;
    typedef value_type&                      reference;

public:
    dtkDistributedMeshConnectivityEntity(dtkDistributedGraphTopologyVertex&& vertex) : m_v(vertex) {}
    dtkDistributedMeshConnectivityEntity(const dtkDistributedMeshConnectivityEntity& o) : m_v(o.m_v) {}

public:
    dtkDistributedMeshConnectivityEntity& operator = (const dtkDistributedMeshConnectivityEntity& o) {
        m_v = o.m_v;
        return *this;
    }

public:
    std::size_t id(void) const { return m_v.id(); }
    std::size_t connectionCount(void) const { return m_v.neighbourCount(); }

    dtkDistributedArray<qlonglong>::const_iterator begin(void) const { return m_v.begin(); }
    dtkDistributedArray<qlonglong>::const_iterator   end(void) const { return m_v.end(); }

public:
    bool operator == (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v == o.m_v);
    }
    bool operator != (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v != o.m_v);
    }
    bool operator <  (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v <  o.m_v);
    }
    bool operator <= (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v <= o.m_v);
    }
    bool operator >  (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v >  o.m_v);
    }
    bool operator >= (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v >= o.m_v);
    }

public:
    dtkDistributedMeshConnectivityEntity& operator ++ (void) {
        ++m_v;
        return *this;
    }
    dtkDistributedMeshConnectivityEntity  operator ++ (int)  {
        dtkDistributedMeshConnectivityEntity o(*this);
        ++m_v;
        return o;
    }
    dtkDistributedMeshConnectivityEntity& operator -- (void) {
        --m_v;
        return *this;
    }
    dtkDistributedMeshConnectivityEntity  operator -- (int)  {
        dtkDistributedMeshConnectivityEntity o(*this);
        --m_v;
        return o;
    }
    dtkDistributedMeshConnectivityEntity& operator += (qlonglong j) {
        m_v += j;
        return *this;
    }
    dtkDistributedMeshConnectivityEntity& operator -= (qlonglong j) {
        m_v -= j;
        return *this;
    }
    dtkDistributedMeshConnectivityEntity  operator +  (qlonglong j) const {
        dtkDistributedMeshConnectivityEntity o(*this);
        o += j;
        return o;
    }
    dtkDistributedMeshConnectivityEntity  operator -  (qlonglong j) const {
        dtkDistributedMeshConnectivityEntity o(*this);
        o -= j;
        return o;
    }

    difference_type operator - (const dtkDistributedMeshConnectivityEntity& o) const {
        return (m_v.id() - o.m_v.id());
    }
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedMeshConnectivity
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYDISTRIBUTED_EXPORT dtkDistributedMeshConnectivity
{
public:
    typedef typename dtkDistributedGraphTopology::Neighbours Connections;

public:
     dtkDistributedMeshConnectivity(void);
     dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim);
     dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, std::size_t entities_count);
     dtkDistributedMeshConnectivity(std::size_t entities_dim, std::size_t connections_dim, dtkDistributedGraphTopology *graph);
     dtkDistributedMeshConnectivity(const dtkDistributedMeshConnectivity& o);
     dtkDistributedMeshConnectivity(dtkDistributedMeshConnectivity&& o);
    ~dtkDistributedMeshConnectivity(void);

public:
    dtkDistributedMeshConnectivity& operator = (const dtkDistributedMeshConnectivity& o);
    dtkDistributedMeshConnectivity& operator = (dtkDistributedMeshConnectivity&& o);

public:
    std::size_t entitiesDimension(void) const;
    std::size_t connectionsDimension(void) const;

    void setEntitiesDimension(std::size_t dim);
    void setConnectionsDimension(std::size_t dim);

public:
    const dtkDistributedGraphTopology *graph(void) const;
          dtkDistributedGraphTopology *graph(void);

    void setGraph(dtkDistributedGraphTopology *graph);

public:
    void clear(void);

    bool empty(void) const;

    std::size_t size(void) const;

    std::size_t size(std::size_t entity_id) const;

public:
    void connect(std::size_t entity_id, std::size_t connected_entity_id);

    Connections operator () (std::size_t entity_id) const;

    void setAssembleFlag(bool is_assembled);
    bool assemble(void);

public:
    std::string toString(bool verbose = false) const;

public:
    dtkDistributedMeshConnectivityEntity begin(void) const;
    dtkDistributedMeshConnectivityEntity   end(void) const;

private:
    std::size_t m_entities_dim;
    std::size_t m_connections_dim;

    dtkDistributedGraphTopology *m_graph;
};

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

DTKDISCRETEGEOMETRYDISTRIBUTED_EXPORT std::ostream& operator << (std::ostream& out, const dtkDistributedMeshConnectivity& mc);

//
// dtkDistributedMeshConnectivity.h ends here
