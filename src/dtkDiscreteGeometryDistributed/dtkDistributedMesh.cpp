// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistributedMesh.h"


#include <dtkMesh>
#include <dtkMeshData>
#include <dtkMeshPartitionMap>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDistributedMesh::dtkDistributedMesh(const dtkMesh *mesh, const dtkMeshPartitionMap *map) : dtkDistributedMesh()
{
    m_comm = dtkDistributed::communicator::instance();
    int wid = m_comm->wid();
    long int topo_dim;

    if (wid == 0) {
        m_geom_dim = mesh->data()->geometricalDimension();
        topo_dim = mesh->data()->topologicalDimension();
    }
    m_comm->broadcast(&m_geom_dim, 1, 0);
    m_comm->broadcast(&topo_dim, 1, 0);

    m_entity_counts.resize(topo_dim + 1);
    if (wid == 0) {
        m_entity_counts[0] = mesh->data()->pointsCount();
        for (int i = 1; i < m_entity_counts.size(); ++i) {
            m_entity_counts[i] = mesh->data()->cellsCount(i);
        }
    }
    auto entity_count_ptr = m_entity_counts.data();
    m_comm->broadcast(entity_count_ptr, m_entity_counts.size(), 0);

    m_connectivities.resize(topo_dim + 1);
    for (std::size_t d0 = 0; d0 <= topo_dim; ++d0) {
        m_connectivities[d0].reserve(topo_dim + 1);
        for (std::size_t d1 = 0; d1 <= topo_dim; ++d1) {
            m_connectivities[d0].push_back(dtkDistributedMeshConnectivity(d0, d1));
        }
    }
    m_comm->barrier();

    dtkDistributedArray<long long int> *cell_vertex_connection_count = new dtkDistributedArray<long long int>(m_comm->size(), 0LL);
    dtkDistributedArray<long long int> *cell_per_part = new dtkDistributedArray<long long int>(m_comm->size(), 0LL);
    dtkDistributedArray<long long int> *cell_old_to_new_ids = new dtkDistributedArray<long long int>(m_entity_counts[topo_dim], 0LL);
    dtkDistributedArray<long long int> *cell_new_to_old_ids = new dtkDistributedArray<long long int>(m_entity_counts[topo_dim], 0LL);

    long int total_connections = 0UL;
    if (wid == 0) {
        cell_old_to_new_ids->wlock();
        cell_new_to_old_ids->wlock();
        cell_per_part->wlock();
        cell_vertex_connection_count->wlock();

        auto *epart = map->elementsPartition();
        auto  esize = map->elementCount();
        int vertex_count;
        const long long int *vertex_ids;

        std::vector<long long int> local_cell_per_part(m_comm->size(), 0);
        std::vector<long long int> local_connection_count(m_comm->size(), 0);
        for (std::size_t i = 0; i < esize; ++i) {
            cell_old_to_new_ids->setAt(i, local_cell_per_part[epart[i]]);

            local_cell_per_part[epart[i]] += 1LL;

            mesh->data()->cellPoints(i, vertex_count, vertex_ids);
            local_connection_count[epart[i]] += vertex_count;

            total_connections += vertex_count;
        }

        for (int i = 0; i < m_comm->size(); ++i) {
            cell_per_part->setAt(i, local_cell_per_part[i]);
            cell_vertex_connection_count->setAt(i, local_connection_count[i]);
        }

        cell_old_to_new_ids->unlock();
        cell_new_to_old_ids->unlock();
        cell_per_part->unlock();
        cell_vertex_connection_count->unlock();
    }
    m_comm->broadcast(&total_connections, 1, 0);

    dtkDistributedMapper *cell_mapper = new dtkDistributedMapper;
    cell_mapper->initMap(m_entity_counts[topo_dim], m_comm->size());
    long long int offset = 0;

    cell_per_part->rlock();
    for (int i = 0; i < m_comm->size(); ++i) {
        cell_mapper->setMap(offset , i);
        offset += cell_per_part->at(i);
    }
    cell_per_part->unlock();
    m_comm->barrier();
    delete cell_per_part;

    if (wid == 0) {
        cell_old_to_new_ids->wlock();
        cell_new_to_old_ids->wlock();

        auto *epart = map->elementsPartition();
        auto  esize = map->elementCount();
        for (std::size_t i = 0; i < esize; ++i) {
            offset = cell_mapper->firstIndex(epart[i]);
            cell_old_to_new_ids->addAssign(i, offset);
        }
        cell_old_to_new_ids->unlock();
        cell_old_to_new_ids->rlock();

        for (std::size_t i = 0; i < esize; ++i) {
            long long int new_id = cell_old_to_new_ids->at(i);
            cell_new_to_old_ids->setAt(new_id, i);
        }

        cell_old_to_new_ids->unlock();
        cell_new_to_old_ids->unlock();
    }

    dtkDistributedGraphTopology *ctv_graph = new dtkDistributedGraphTopology(m_entity_counts[topo_dim], cell_mapper);
    m_connectivities[topo_dim][0].setGraph(ctv_graph);

    ctv_graph->setEdgeCountArray(cell_vertex_connection_count);

    dtkDistributedArray<long long int> old_cell_to_vertex(m_entity_counts[topo_dim], 0LL);
    dtkDistributedArray<long long int> old_connections(m_entity_counts[topo_dim] + total_connections, 0LL);
    if (wid == 0) {
        old_cell_to_vertex.wlock();
        old_connections.wlock();

        long long int local_cell_count;
        long long int *local_cells_pos = const_cast<long long int *>(mesh->data()->cellsIdx(local_cell_count, topo_dim));
        long long int local_connections_count;
        long long int *local_connections = const_cast<long long int *>(mesh->data()->cellsPoints(local_connections_count, topo_dim));

        long long int entity_count = m_entity_counts[topo_dim];
        for (int i = 0; i < m_comm->size(); ++i) {
            long long int pos = old_cell_to_vertex.mapper()->firstIndex(i);
            long long int *ptr = local_cells_pos + pos;
            long long int size = old_cell_to_vertex.mapper()->count(i);
            old_cell_to_vertex.setAt(pos, ptr, size);

            pos = old_connections.mapper()->firstIndex(i);
            ptr = local_connections + pos;
            size = old_connections.mapper()->count(i);
            old_connections.setAt(pos, ptr, size);
        }

        old_cell_to_vertex.unlock();
        old_connections.unlock();
    }
    m_comm->barrier();

    //
    dtkDistributedMapper *ctv_edge_to_vertex_mapper = new dtkDistributedMapper;
    ctv_edge_to_vertex_mapper->initMap(total_connections, m_comm->size());
    offset = 0;
    for (int i = 0; i < m_comm->size(); ++i) {
        ctv_edge_to_vertex_mapper->setMap(offset, i);
        offset += cell_vertex_connection_count->at(i);
    }

    auto ctv_node_array = ctv_graph->vertexToEdgeArray();
    auto ctv_neighbour_count_array = ctv_graph->neighbourCountArray();

    dtkDistributedArray<long long int> new_connections_old_vid(total_connections, ctv_edge_to_vertex_mapper);
    new_connections_old_vid.fill(-1LL);

    long long int counter = ctv_graph->mapper()->firstIndex(wid);
    auto it = ctv_node_array->begin();
    auto end = ctv_node_array->end();
    auto nit = ctv_neighbour_count_array->begin();
    offset = 0;
    for (int i = 0; i < wid; ++i) {
        offset += cell_vertex_connection_count->at(i);
    }
    if (wid == m_comm->size()-1) {
        end = ctv_node_array->end() -1;
    }
    old_connections.rlock();
    //new_connections_old_vid.wlock();
    old_cell_to_vertex.rlock();
    for (; it != end; ++it, ++counter, ++nit) {
        long long int old_id = cell_new_to_old_ids->at(counter);
        auto pos = old_cell_to_vertex.at(old_id);
        auto vertex_count = old_connections.at(pos);
        ++pos;
        *it = offset;
        for (int i = 0; i < vertex_count; ++i) {
            auto vid = old_connections.at(pos + i);
            new_connections_old_vid.setAt(offset, vid);
            ++offset;
        }
        *nit = vertex_count;
    }
    old_connections.unlock();
    //new_connections_old_vid.unlock();
    old_cell_to_vertex.unlock();

    delete cell_old_to_new_ids;
    delete cell_new_to_old_ids;

    dtkDistributedArray<long long int> *vert_per_part = new dtkDistributedArray<long long int>(m_comm->size(), 0LL);
    dtkDistributedArray<int> *vert_owner = new dtkDistributedArray<int>(m_entity_counts[0], -1LL);

    counter = ctv_graph->mapper()->firstIndex(wid);
    it = ctv_node_array->begin();
    end = ctv_node_array->end();
    nit = ctv_neighbour_count_array->begin();
    if (wid == m_comm->size()-1) {
        end = ctv_node_array->end() -1;
    }
    long long int vid;
    new_connections_old_vid.rlock();

    int minusOne = -1LL;
    QMap<long long int, int> ext_owner;
    for (; it != end; ++it, counter++, ++nit) {
        auto pos = *it;
        auto vertex_count = *nit;
        for (int i = 0; i < vertex_count; ++i) {
            vid = new_connections_old_vid.at(pos);
            if (!vert_owner->compareAndSwap(vid, wid, minusOne)) {
                auto owner = vert_owner->at(vid);
                if (wid < owner) {
                    ext_owner[vid] = owner;
                }
            }
            ++pos;
        }
    }
    new_connections_old_vid.unlock();
    m_comm->barrier();

    while (!ext_owner.empty()) {
        auto ext_it = ext_owner.begin();
        auto ext_end = ext_owner.end();
        while (ext_it != ext_end) {
            int owner = *ext_it;
            if (wid > owner) {
                auto next = ext_owner.erase(ext_it);
                ext_it = next;
            } else {
                vid = ext_it.key();
                if (!vert_owner->compareAndSwap(vid, wid, owner)) {
                    owner = vert_owner->at(vid);
                    *ext_it = owner;
                    ++ext_it;
                } else {
                    auto next = ext_owner.erase(ext_it);
                    ext_it = next;
                }
            }
        }
    }
    m_comm->barrier();

    {
        std::vector<int> vert_per_part_computed_by_current_partition(m_comm->size(), 0LL);
        std::vector<int> vert_per_part_reduced(m_comm->size(), 0LL);
        auto vit = vert_owner->begin();
        auto vend = vert_owner->end();
        for (; vit != vend; ++vit) {
            vert_per_part_computed_by_current_partition[*vit]++;
        }
        m_comm->reduce(vert_per_part_computed_by_current_partition.data(), vert_per_part_reduced.data(), vert_per_part_computed_by_current_partition.size(), dtkDistributedCommunicator::Sum, 0, true);

        vert_per_part->setAt(wid, vert_per_part_reduced[wid]);
    }

    m_comm->barrier();

    dtkDistributedArray<long long int> *old_to_new_vids = new dtkDistributedArray<long long int>(m_entity_counts[0], 0LL);
    std::vector<long long int> offset_per_owner(m_comm->size(), 0LL);

    {
        auto vowner_it = vert_owner->begin();
        auto vit = old_to_new_vids->begin();
        auto vend = old_to_new_vids->end();

        vert_per_part->rlock();
        for (std::size_t i = 0; i < m_comm->size(); ++i) {
            for (int j = 0; j < i; ++j) {
                offset_per_owner[i] += vert_per_part->at(j);
            }
        }
        vert_per_part->unlock();

        for (;vit != vend; ++vit, ++vowner_it) {
            *vit = offset_per_owner[*vowner_it];
        }

    }
    m_comm->barrier();

    {
        auto vit = new_connections_old_vid.begin();
        auto vend = new_connections_old_vid.end();

        QHash<long long int, bool> has_already_been_treated;
        offset = offset_per_owner[wid];
        vert_owner->rlock();
        //old_to_new_vids->wlock();
        for (; vit != vend; ++vit) {
            auto old_id = *vit;
            if (wid == vert_owner->at(old_id) && !has_already_been_treated.contains(old_id)) {
                has_already_been_treated.insert(old_id, true);
                old_to_new_vids->setAt(old_id, offset);
                ++offset;
            }
        }
        vert_owner->unlock();
        //old_to_new_vids->unlock();
    }
    delete vert_owner;

    m_new_connections_new_vid = new dtkDistributedArray<long long int>(total_connections, ctv_edge_to_vertex_mapper);

    {
        auto it = m_new_connections_new_vid->begin();
        auto end = m_new_connections_new_vid->end();
        auto old_it = new_connections_old_vid.begin();
        old_to_new_vids->rlock();
        for (; it != end; ++it, ++old_it) {
            *it = old_to_new_vids->at(*old_it);
        }
        old_to_new_vids->unlock();
    }
    m_comm->barrier();

    ctv_graph->setEdgeToVertexArray(m_new_connections_new_vid);
    ctv_graph->setAssembleFlag(true);

    dtkDistributedMapper *vert_mapper = new dtkDistributedMapper;
    vert_mapper->initMap(m_entity_counts[0], m_comm->size());
    offset = 0;
    vert_per_part->rlock();
    for (int i = 0; i < m_comm->size(); ++i) {
        vert_mapper->setMap(offset , i);
        offset += vert_per_part->at(i);
    }
    vert_per_part->unlock();
    delete vert_per_part;

    dtkDistributedArray<long long int> *new_to_old_vids = new dtkDistributedArray<long long int>(m_entity_counts[0], vert_mapper);

    {
        //new_to_old_vids->wlock();
        auto vit = old_to_new_vids->begin();
        auto vend = old_to_new_vids->end();
        offset = old_to_new_vids->mapper()->firstIndex(wid);
        for (; vit != vend; ++vit, ++offset) {
            new_to_old_vids->setAt(*vit, offset);
        }
        //new_to_old_vids->unlock();
    }
    m_comm->barrier();

    // New coordinates
    dtkDistributedMapper *coord_mapper = vert_mapper->scaledClone(3LL); // m_geom_dimm a la place quand elle sera debuggée
    m_coords = new dtkDistributedArray<double>(3 * m_entity_counts[0], coord_mapper);

    //m_coords->wlock();
    if (wid == 0){
        new_to_old_vids->rlock();
        const double *point_coordinates = mesh->data()->pointsCoordinates();

        for (std::size_t i = 0; i <  m_entity_counts[0]; ++i) {
            double x = point_coordinates[3*new_to_old_vids->at(i)];
            double y = point_coordinates[3*new_to_old_vids->at(i)+1];
            double z = point_coordinates[3*new_to_old_vids->at(i)+2];
            m_coords->setAt(3*i,x);
            m_coords->setAt(3*i+1,y);
            m_coords->setAt(3*i+2,z);
        }
        new_to_old_vids->unlock();
    }
    //m_coords->unlock();
    m_comm->barrier();

    // Vertext to cell connections initialization
    dtkDistributedGraphTopology *vtc_graph = new dtkDistributedGraphTopology(m_entity_counts[0], vert_mapper);
    m_connectivities[0][topo_dim].setGraph(vtc_graph);

    delete old_to_new_vids;
    delete new_to_old_vids;

    this->buildConnectivities();
}

dtkDistributedMesh::~dtkDistributedMesh(void)
{
    if (m_coords) {
        delete m_coords;
    }

    m_connectivities.clear();
}

void dtkDistributedMesh::buildConnectivities(void)
{
    int wid = m_comm->wid();

    int topo_dim = m_entity_counts.size() - 1;

    // Now we want to build connectivity from vertices to entity of
    // higher topological dimension, ie the connectivity
    // [0][topo_dim]. For that, we use the connectivity [topo_dim][0].

    dtkDistributedGraphTopology *ctv_graph = m_connectivities[topo_dim][0].graph();
    dtkDistributedGraphTopology *vtc_graph = m_connectivities[0][topo_dim].graph();

    // Firstly, we loop over all the local entities of higher
    // topological dimension (aka elements) and we store information
    // into 3 structures:
    //
    // - vtc_neighbour_count_array : a distributed array that stores
    //  for each vertex the number of elements to which it is
    //  connected
    // - local_vtc_neighbour_ids : a local array that contains for
    //   each vertex owned by the current partition the list of the
    //   local element ids to which it is connected
    // - exterior_vtc_neighbour_ids : a local map that contains for
    //   each vertex that is not owned by the current partition the
    //   list of the local element ids to which it is connected

    auto *vtc_neighbour_count_array = vtc_graph->neighbourCountArray();

    dtkDistributedGraphTopologyVertex elt_it  = ctv_graph->beginVertex();
    dtkDistributedGraphTopologyVertex elt_end = ctv_graph->endVertex();

    std::vector< std::list<long long int> > local_vtc_neighbour_ids(vtc_graph->mapper()->count(wid));
    std::map<long long int, std::list<long long int> > exterior_vtc_neighbour_ids;

    long long int offset = vtc_graph->mapper()->firstIndex(wid);

    // loop over local elements
    for (; elt_it != elt_end; ++elt_it) {

        auto v_it  = elt_it.begin();
        auto v_end = elt_it.end();

        auto elt_id = elt_it.id();

        // loop over all the vertices of the current element
        for (; v_it != v_end; ++v_it) {
            auto v_id = *v_it;
            vtc_neighbour_count_array->addAssign(v_id, 1);
            if (vtc_graph->mapper()->owner(v_id) == wid) {
                local_vtc_neighbour_ids[v_id - offset].push_back(elt_id);
            } else {
                exterior_vtc_neighbour_ids[v_id].push_back(elt_id);
            }
        }
    }

    // Secondly, we build the distributed array that contains for each
    // partition the local number of connections between vertices and
    // elements. To achieve this task, we loop over all the local
    // vertex ids of the distributed array vtc_neighbour_count_array
    // and we sum each entry into the variable local_vtc_edge_count.

    dtkDistributedArray<qlonglong> *vtc_edge_count_array = new dtkDistributedArray<qlonglong>(m_comm->size(), 0LL);
    vtc_graph->setEdgeCountArray(vtc_edge_count_array);
    long long int local_vtc_edge_count = 0;
    {
        auto vit  = vtc_neighbour_count_array->begin();
        auto vend = vtc_neighbour_count_array->end();
        for (; vit != vend; ++vit) {
            local_vtc_edge_count += *vit;
        }
        vtc_edge_count_array->setAt(wid, local_vtc_edge_count);
    }

    // Furthermore, we compute the global number of connections
    // between vertices and elements using a reduction.

    long long int total_connections;
    m_comm->reduce(&local_vtc_edge_count, &total_connections, 1LL, dtkDistributedCommunicator::Sum, 0, true);

    // We are now able to build the mapper of the distributed array
    // that will contain all the element ids to which each vertex is
    // connected.

    dtkDistributedMapper *vtc_edge_to_vertex_mapper = new dtkDistributedMapper;
    vtc_edge_to_vertex_mapper->initMap(total_connections, m_comm->size());
    offset = 0;
    for (int i = 0; i < m_comm->size(); ++i) {
        vtc_edge_to_vertex_mapper->setMap(offset, i);
        offset += vtc_edge_count_array->at(i);
    }

    m_comm->barrier();

    // Using both the above mapper and the distributed array
    // vtc_neighbour_count_array, we can fill the distributed array
    // vtc_vertex_to_edge_array that contains for each vertex the
    // position of the first element id to which it is connected into
    // the distributed array vtc_edge_count_array.

    auto *vtc_vertex_to_edge_array = vtc_graph->vertexToEdgeArray();

    {
        offset = vtc_edge_to_vertex_mapper->firstIndex(wid);

        auto vtc_vte_it  = vtc_vertex_to_edge_array->begin();
        auto vtc_vte_end = vtc_vertex_to_edge_array->end();

        auto vtc_nc_it = vtc_neighbour_count_array->begin();

        // For each part, the first entry of the
        // vtc_vertex_to_edge_array is equal to the sum of all the
        // connections of the previous parts. It means that it is
        // equal to the first index given by the
        // vtc_edge_to_vertex_mapper for each partition.
        *vtc_vte_it = offset;
        ++vtc_vte_it;

        // For the remaining entries, the value is equal to the
        // previous value plus the number of connections of the
        // previous vertex given by the vtc_neighbour_count_array.
        for (; vtc_vte_it != vtc_vte_end; ++vtc_vte_it, ++vtc_nc_it) {
            offset += *vtc_nc_it;
            *vtc_vte_it = offset;
        }
        if (wid == m_comm->size()-1) {
            Q_ASSERT_X((total_connections == vtc_vertex_to_edge_array->at(m_entity_counts[0])), "", "Last entry of vtc_vertex_to_edge_array is not equal to the total number of connections");
        }
    }

    // Now, we can fill the distibuted array vtc_edge_to_vertex_array
    // that contains all the element ids to which each vertex is
    // connected.

    dtkDistributedArray<long long int> *vtc_edge_to_vertex_array = new dtkDistributedArray<long long int>(total_connections, vtc_edge_to_vertex_mapper);
    vtc_graph->setEdgeToVertexArray(vtc_edge_to_vertex_array);
    vtc_edge_to_vertex_array->fill(-1LL);

    {
        // Firstly, each partition fills its part of the
        // vtc_edge_to_vertex_array using the local_vtc_neighbour_ids
        // array that provides for each vertex the list of the local
        // element ids to which it is connected.
        auto vtc_etv_it  = vtc_edge_to_vertex_array->begin();
        auto vtc_etv_end = vtc_edge_to_vertex_array->end();

        auto vtc_nc_it = vtc_neighbour_count_array->begin();

        // In practice, we go through the local part of the
        // vtc_edge_to_vertex_array. As some vertex are connected to
        // elements that does not belong to the current partition,
        // some entries cannot be filled so we have to skip them in
        // order to go to the first entry of the next vertex.
        for (std::size_t i = 0; i < local_vtc_neighbour_ids.size(); ++i, ++vtc_nc_it) {
            auto elt_id_list = local_vtc_neighbour_ids[i];
            for (auto elt_id : elt_id_list) {
                *vtc_etv_it = elt_id;
                ++vtc_etv_it;
            }
            vtc_etv_it += *vtc_nc_it - elt_id_list.size(); // Skip entries related to exterior elements
        }
        m_comm->barrier();

        // Now, each partition fills the vtc_edge_to_vertex_array
        // using its local map exterior_vtc_neighbour_ids that stores
        // for each vertex that does not belong to the current
        // partition the list of the local element ids to which it is
        // connected.
        auto ext_it  = exterior_vtc_neighbour_ids.begin();
        auto ext_end = exterior_vtc_neighbour_ids.end();

        long long int minusOne = -1LL;

        // In practice, we find the position of the first element
        // connected to the current vertex in the distributed array
        // vtc_edge_to_vertex_array. Then, we look at the first entry
        // that stores the value -1 and we switch it with the element
        // id given by the list of elements related to the current
        // vertex.
        vtc_vertex_to_edge_array->rlock();
        vtc_neighbour_count_array->rlock();
        for (; ext_it != ext_end; ++ext_it) {
            auto v_id = ext_it->first;
            auto elt_list = ext_it->second;
            auto pos = vtc_vertex_to_edge_array->at(v_id);
            auto elt_count = vtc_neighbour_count_array->at(v_id);
            for (auto elt_id : elt_list) {
                while (!vtc_edge_to_vertex_array->compareAndSwap(pos, elt_id, minusOne)) {
                    ++pos;
                }
            }
        }
        vtc_vertex_to_edge_array->unlock();
        vtc_neighbour_count_array->unlock();
    }
    vtc_graph->setAssembleFlag(true);
    m_comm->barrier();
}

const dtkDistributedMeshConnectivity& dtkDistributedMesh::connectivity(std::size_t entities_dim, std::size_t connections_dim) const
{
    return m_connectivities[entities_dim][connections_dim];
}

dtkDistributedMeshConnectivity& dtkDistributedMesh::connectivity(std::size_t entities_dim, std::size_t connections_dim)
{
    return m_connectivities[entities_dim][connections_dim];
}

//
// dtkDistributedMesh.cpp ends here
