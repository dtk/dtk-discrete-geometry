// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryCore.h"

#include "dtkComposerNodeMeshVolumesExtractor.h"

#include "dtkMesh.h"

#include "dtkMeshCollection.h"

class dtkComposerNodeMeshVolumesExtractorPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh*> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMeshCollection*> emt_out_vol_meshes;
};

dtkComposerNodeMeshVolumesExtractor::dtkComposerNodeMeshVolumesExtractor(void) : dtkComposerNodeObject< dtkMeshVolumesExtractor >(), d(new dtkComposerNodeMeshVolumesExtractorPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshVolumesExtractor::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_vol_meshes);
}

dtkComposerNodeMeshVolumesExtractor::~dtkComposerNodeMeshVolumesExtractor(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshVolumesExtractor::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshVolumesExtractor instantiated, abort:" << this->currentImplementation();
            d->emt_out_vol_meshes.clearData();
            return;
        }

        dtkMeshVolumesExtractor *process = this->object();
        process->setMesh(d->rcv_in_mesh.constData());

        process->run();

        d->emt_out_vol_meshes.setData(process->meshCollection());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_vol_meshes.clearData();
        return;
    }
}

//
// dtkComposerNodeMeshVolumesExtractor.cpp ends here
