// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkDiscreteGeometryCore/dtkMeshReader>

#include <dtkComposer/dtkComposerNodeObject>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshReader : public dtkComposerNodeObject< dtkMeshReader >
{
public:
     dtkComposerNodeMeshReader(void);
    ~dtkComposerNodeMeshReader(void);

public:
    void run(void);

private:
    class dtkComposerNodeMeshReaderPrivate *d;
};

//
// dtkComposerNodeMeshReader.h ends here
