// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeMeshCurvatureEstimator.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

class dtkComposerNodeMeshCurvatureEstimatorPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
};

dtkComposerNodeMeshCurvatureEstimator::dtkComposerNodeMeshCurvatureEstimator(void) : dtkComposerNodeObject< dtkMeshCurvatureEstimator >(), d(new dtkComposerNodeMeshCurvatureEstimatorPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshCurvatureEstimator::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_mesh);
}

dtkComposerNodeMeshCurvatureEstimator::~dtkComposerNodeMeshCurvatureEstimator(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshCurvatureEstimator::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshLabeler instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            return;
        }

        dtkMesh* mesh_in = d->rcv_in_mesh.constData();
        dtkMeshCurvatureEstimator *process = this->object();
        process->setMesh(mesh_in);

        process->run();
        d->emt_out_mesh.setData(process->mesh());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_mesh.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshCurvatureEstimator.cpp ends here
