// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeMeshCollectionToMeshList.h"

#include "dtkMesh.h"
#include "dtkMeshCollection.h"

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeMeshCollectionToMeshListPrivate
// /////////////////////////////////////////////////////////////////

class dtkComposerNodeMeshCollectionToMeshListPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMeshCollection *> rcv_in_meshes_collection;

public:
    dtkComposerTransmitterEmitter<QList<dtkMesh *> *> emt_out_meshes_list;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeMeshCollectionToMeshList
// /////////////////////////////////////////////////////////////////

dtkComposerNodeMeshCollectionToMeshList::dtkComposerNodeMeshCollectionToMeshList(void) : dtkComposerNodeLeaf(), d(new dtkComposerNodeMeshCollectionToMeshListPrivate())
{
    this->appendReceiver(&d->rcv_in_meshes_collection);

    this->appendEmitter(&d->emt_out_meshes_list);
}

dtkComposerNodeMeshCollectionToMeshList::~dtkComposerNodeMeshCollectionToMeshList(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshCollectionToMeshList::run(void)
{
    if (!d->rcv_in_meshes_collection.isEmpty()) {
        d->emt_out_meshes_list.setData(d->rcv_in_meshes_collection.constData()->values());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_meshes_list.clearData();
        return;
    }
}

//
// dtkComposerNodeMeshCollectionToMeshList.cpp ends here
