// Version: $Id: c1e080611a900435111247d22e96914e40b6b262 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkMeshesComparator.h>

class dtkComposerNodeMeshesComparatorPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshesComparator : public dtkComposerNodeObject< dtkMeshesComparator >
{
public:
    dtkComposerNodeMeshesComparator(void);
    ~dtkComposerNodeMeshesComparator(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshesComparatorPrivate *d;
};

//
// dtkComposerNodeMeshesComparator.h ends here
