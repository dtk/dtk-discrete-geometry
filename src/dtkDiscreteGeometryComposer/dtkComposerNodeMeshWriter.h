// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkMeshWriter.h>

class dtkComposerNodeMeshWriterPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshWriter : public dtkComposerNodeObject< dtkMeshWriter >
{
public:
    dtkComposerNodeMeshWriter(void);
    ~dtkComposerNodeMeshWriter(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshWriterPrivate *d;
};

//
// dtkComposerNodeMeshWriter.h ends here
