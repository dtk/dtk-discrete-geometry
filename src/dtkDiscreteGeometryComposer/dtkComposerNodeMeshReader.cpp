// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeMeshReader.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

class dtkComposerNodeMeshReaderPrivate
{
public:
    dtkComposerTransmitterReceiver<QString> rcv_in_file;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
};

dtkComposerNodeMeshReader::dtkComposerNodeMeshReader(void) : dtkComposerNodeObject< dtkMeshReader >(), d(new dtkComposerNodeMeshReaderPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshReader::pluginFactory());

    this->appendReceiver(&d->rcv_in_file);

    this->appendEmitter(&d->emt_out_mesh);
}

dtkComposerNodeMeshReader::~dtkComposerNodeMeshReader(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshReader::run(void)
{
    if (!d->rcv_in_file.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshReader instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            return;
        }

        const QString file = d->rcv_in_file.constData();
        dtkMeshReader *process = this->object();
        process->setMeshFile(file);

        process->run();
        d->emt_out_mesh.setData(process->mesh());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_mesh.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshReader.cpp ends here
