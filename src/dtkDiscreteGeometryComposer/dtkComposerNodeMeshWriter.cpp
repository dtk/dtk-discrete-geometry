// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryCore.h"

#include "dtkComposerNodeMeshWriter.h"

#include <dtkMesh>

class dtkComposerNodeMeshWriterPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh*> rcv_in_mesh;
    dtkComposerTransmitterReceiver<QString> rcv_out_file;

public:
    dtkComposerTransmitterEmitter<QString> emt_out_file;
};

dtkComposerNodeMeshWriter::dtkComposerNodeMeshWriter(void) : dtkComposerNodeObject< dtkMeshWriter >(), d(new dtkComposerNodeMeshWriterPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshWriter::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);
    this->appendReceiver(&d->rcv_out_file);

    this->appendEmitter(&d->emt_out_file);
}

dtkComposerNodeMeshWriter::~dtkComposerNodeMeshWriter(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshWriter::run(void)
{
    if (!d->rcv_in_mesh.isEmpty() && d->rcv_in_mesh.constData() != nullptr) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshWriter instantiated, abort:" << this->currentImplementation();
            d->emt_out_file.clearData();
            return;
        }

        dtkMeshWriter *process = this->object();
        process->setInputDtkMesh(d->rcv_in_mesh.constData());
        process->setOutputMeshFile(d->rcv_out_file.constData());

        process->run();
        d->emt_out_file.setData(process->outputMeshFile());
    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_file.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshWriter.cpp ends here
