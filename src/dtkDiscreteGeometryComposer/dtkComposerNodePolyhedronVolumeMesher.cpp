// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodePolyhedronVolumeMesher.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

class dtkComposerNodePolyhedronVolumeMesherPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

dtkComposerNodePolyhedronVolumeMesher::dtkComposerNodePolyhedronVolumeMesher(void) : dtkComposerNodeObject< dtkPolyhedronVolumeMesher >(), d(new dtkComposerNodePolyhedronVolumeMesherPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::polyhedronVolumeMesher::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_mesh);

    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodePolyhedronVolumeMesher::~dtkComposerNodePolyhedronVolumeMesher(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodePolyhedronVolumeMesher::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No mesher instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            d->emt_stat.clearData();
            return;
        }

        dtkPolyhedronVolumeMesher *mesher = this->object();
        mesher->setMesh(d->rcv_in_mesh.constData());

        mesher->run();

        d->emt_out_mesh.setData(mesher->mesh());
        d->emt_stat.setData(mesher->statistics());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The necessary input are not set. Nothing is done.";
        d->emt_out_mesh.clearData();
        d->emt_stat.clearData();
        return;
    }
}
//
// dtkComposerNodePolyhedronVolumeMesher.cpp ends here
