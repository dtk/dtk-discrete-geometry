// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryCore.h"

#include "dtkComposerNodeMeshSurfacesExtractor.h"

#include "dtkMesh.h"

#include "dtkMeshCollection.h"

class dtkComposerNodeMeshSurfacesExtractorPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh*> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMeshCollection*> emt_out_surf_meshes;
};

dtkComposerNodeMeshSurfacesExtractor::dtkComposerNodeMeshSurfacesExtractor(void) : dtkComposerNodeObject< dtkMeshSurfacesExtractor >(), d(new dtkComposerNodeMeshSurfacesExtractorPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshSurfacesExtractor::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_surf_meshes);
}

dtkComposerNodeMeshSurfacesExtractor::~dtkComposerNodeMeshSurfacesExtractor(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshSurfacesExtractor::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshSurfacesExtractor instantiated, abort:" << this->currentImplementation();
            d->emt_out_surf_meshes.clearData();
            return;
        }

        dtkMeshSurfacesExtractor *process = this->object();
        process->setMesh(d->rcv_in_mesh.constData());

        process->run();

        d->emt_out_surf_meshes.setData(process->meshCollection());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_surf_meshes.clearData();
        return;
    }
}

//
// dtkComposerNodeMeshSurfacesExtractor.cpp ends here
