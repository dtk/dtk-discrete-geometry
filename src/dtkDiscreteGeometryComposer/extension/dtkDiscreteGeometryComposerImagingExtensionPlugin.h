// Version: $Id$
//
//

// Commentary:
//
//

//Author: Nicolas Niclausse
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerImagingExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

#include <QtCore>

class DTKDISCRETEGEOMETRYCOMPOSERIMAGINGEXTENSION_EXPORT dtkDiscreteGeometryComposerImagingExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkDiscreteGeometryComposerImagingExtensionPlugin" FILE "dtkDiscreteGeometryComposerImagingExtensionPlugin.json")

public:
    dtkDiscreteGeometryComposerImagingExtensionPlugin(void) {}
    ~dtkDiscreteGeometryComposerImagingExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};

//
// dtkDiscreteGeometryComposerImagingExtensionPlugin.h ends here
