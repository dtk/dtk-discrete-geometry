// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include <dtkDiscreteGeometryComposerImagingExtension.h>

#include <dtkComposerNodeImageVolumeMesher.h>

#include <dtkComposer/dtkComposerNodeFactory.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDiscreteGeometryComposerImagingExtension::dtkDiscreteGeometryComposerImagingExtension(void) : dtkComposerExtension()
{

}

dtkDiscreteGeometryComposerImagingExtension::~dtkDiscreteGeometryComposerImagingExtension(void)
{

}

void dtkDiscreteGeometryComposerImagingExtension::extend(dtkComposerNodeFactory *factory)
{
    factory->record(":dtkComposer/dtkComposerNodeImageVolumeMesher.json", dtkComposerNodeCreator< dtkComposerNodeImageVolumeMesher >);
}

//
// dtkDiscreteGeometryComposerImagingExtension.cpp ends here
