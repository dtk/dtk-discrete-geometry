// Version: $Id: 1e79f4fa799a123ea455879ae03c89d8ac844a2f $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryComposerExtension.h"

#include "dtkComposerNodeMeshCollectionToMeshList.h"
#include "dtkComposerNodeMeshReader.h"
#include "dtkComposerNodeMeshWriter.h"
#include "dtkComposerNodeMeshVolumesExtractor.h"
#include "dtkComposerNodeMeshSurfacesExtractor.h"
#include "dtkComposerNodeMeshStatisticsExtractor.h"
#include "dtkComposerNodeMeshesComparator.h"
#include "dtkComposerNodeMeshOptimizer.h"
#include "dtkComposerNodeMeshCurvatureEstimator.h"
#include "dtkComposerNodeMeshLabeler.h"
#include "dtkComposerNodePolyhedronVolumeMesher.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDiscreteGeometryComposerExtension::dtkDiscreteGeometryComposerExtension(void) : dtkComposerExtension()
{

}

dtkDiscreteGeometryComposerExtension::~dtkDiscreteGeometryComposerExtension(void)
{

}

void dtkDiscreteGeometryComposerExtension::extend(dtkComposerNodeFactory *factory)
{
    if (!factory) {
        dtkError() << "No composer factory, can't extend it with dtkLinearAlgebraSparse nodes ";
        return;
    }
    factory->record(":dtkComposer/dtkComposerNodeMeshCollectionToMeshList.json", dtkComposerNodeCreator< dtkComposerNodeMeshCollectionToMeshList >);
    factory->record(":dtkComposer/dtkComposerNodeMeshReader.json", dtkComposerNodeCreator< dtkComposerNodeMeshReader >);
    factory->record(":dtkComposer/dtkComposerNodeMeshWriter.json", dtkComposerNodeCreator< dtkComposerNodeMeshWriter >);
    factory->record(":dtkComposer/dtkComposerNodeMeshVolumesExtractor.json", dtkComposerNodeCreator< dtkComposerNodeMeshVolumesExtractor >);
    factory->record(":dtkComposer/dtkComposerNodeMeshSurfacesExtractor.json", dtkComposerNodeCreator< dtkComposerNodeMeshSurfacesExtractor >);
    factory->record(":dtkComposer/dtkComposerNodeMeshOptimizer.json", dtkComposerNodeCreator< dtkComposerNodeMeshOptimizer >);
    factory->record(":dtkComposer/dtkComposerNodeMeshStatisticsExtractor.json", dtkComposerNodeCreator< dtkComposerNodeMeshStatisticsExtractor >);
    factory->record(":dtkComposer/dtkComposerNodeMeshesComparator.json", dtkComposerNodeCreator< dtkComposerNodeMeshesComparator >);
    factory->record(":dtkComposer/dtkComposerNodeMeshCurvatureEstimator.json", dtkComposerNodeCreator< dtkComposerNodeMeshCurvatureEstimator >);
    factory->record(":dtkComposer/dtkComposerNodeMeshLabeler.json", dtkComposerNodeCreator< dtkComposerNodeMeshLabeler >);
    factory->record(":dtkComposer/dtkComposerNodePolyhedronVolumeMesher.json", dtkComposerNodeCreator< dtkComposerNodePolyhedronVolumeMesher >);
}

//
// dtkDiscreteGeometryComposerExtension.cpp ends here
