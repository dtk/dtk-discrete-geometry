// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkDiscreteGeometryComposerContinuousGeometryExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

class DTKDISCRETEGEOMETRYCOMPOSERCONTINUOUSGEOMETRYEXTENSION_EXPORT dtkDiscreteGeometryComposerContinuousGeometryExtension : public dtkComposerExtension
{
public:
     dtkDiscreteGeometryComposerContinuousGeometryExtension(void);
    ~dtkDiscreteGeometryComposerContinuousGeometryExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkDiscreteGeometryComposerContinuousGeometryExtension.h ends here
