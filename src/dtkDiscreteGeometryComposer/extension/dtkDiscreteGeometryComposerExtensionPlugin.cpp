/* dtkDiscreteGeometryComposerExtension.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */
//
//


// Code:

#include "dtkDiscreteGeometryComposerExtensionPlugin.h"
#include "dtkDiscreteGeometryComposerExtension.h"

#include <dtkDiscreteGeometryCore>

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension* dtkDiscreteGeometryComposerCreator(void);

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


void dtkDiscreteGeometryComposerExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkDiscreteGeometry", dtkDiscreteGeometryComposerCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkDiscreteGeometry");
    bool verbose = dtkComposer::extension::pluginManager().verboseLoading();
    dtkDiscreteGeometryCore::setVerboseLoading(verbose);
    extension->extend(&(dtkComposer::node::factory()));
    dtkDiscreteGeometryCore::initialize();
}

void dtkDiscreteGeometryComposerExtensionPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkDiscreteGeometryComposerCreator(void)
{
    return new dtkDiscreteGeometryComposerExtension();
}
