// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryComposerContinuousGeometryExtension.h"

#include "dtkComposerNodeBrepVolumeMesher.h"

#include <dtkComposer/dtkComposerNodeFactory.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDiscreteGeometryComposerContinuousGeometryExtension::dtkDiscreteGeometryComposerContinuousGeometryExtension(void) : dtkComposerExtension()
{

}

dtkDiscreteGeometryComposerContinuousGeometryExtension::~dtkDiscreteGeometryComposerContinuousGeometryExtension(void)
{

}

void dtkDiscreteGeometryComposerContinuousGeometryExtension::extend(dtkComposerNodeFactory *factory)
{
    factory->record(":dtkComposer/dtkComposerNodeBrepVolumeMesher.json", dtkComposerNodeCreator< dtkComposerNodeBrepVolumeMesher >);
}

//
// dtkDiscreteGeometryComposerContinuousGeometryExtension.cpp ends here
