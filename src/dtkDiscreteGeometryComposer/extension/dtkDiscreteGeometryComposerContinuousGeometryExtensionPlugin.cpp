// Version: $Id$
//
//

// Commentary:
//
//

// Author : Nicolas Niclause
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin.h"
#include "dtkDiscreteGeometryComposerContinuousGeometryExtension.h"

#include <dtkDiscreteGeometryCore>

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension* dtkDiscreteGeometryComposerContinuousGeometryCreator(void);

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


void dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkDiscreteGeometryContinuousGeometry", dtkDiscreteGeometryComposerContinuousGeometryCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkDiscreteGeometryContinuousGeometry");
    extension->extend(&(dtkComposer::node::factory()));
}

void dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin::uninitialize(void)
{

}


// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkDiscreteGeometryComposerContinuousGeometryCreator(void)
{
    return new dtkDiscreteGeometryComposerContinuousGeometryExtension();
}

//
// dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin.cpp ends here
