// Version: $Id$
//
//

// Commentary:
//
//

//Author: Nicolas Niclausse
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerContinuousGeometryExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

#include <QtCore>

class DTKDISCRETEGEOMETRYCOMPOSERCONTINUOUSGEOMETRYEXTENSION_EXPORT dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin" FILE "dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin.json")

public:
    dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin(void) {}
    ~dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};

//
// dtkDiscreteGeometryComposerContinuousGeometryExtensionPlugin.h ends here
