// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSEREXTENSION_EXPORT dtkDiscreteGeometryComposerExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkDiscreteGeometryComposerExtensionPlugin" FILE "dtkDiscreteGeometryComposerExtensionPlugin.json")

public:
    dtkDiscreteGeometryComposerExtensionPlugin(void) {}
    ~dtkDiscreteGeometryComposerExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};

//
// dtkDiscreteGeometryComposerExtensionPlugin.h ends here
