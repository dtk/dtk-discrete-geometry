// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkDiscreteGeometryComposerImagingExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

class DTKDISCRETEGEOMETRYCOMPOSERIMAGINGEXTENSION_EXPORT dtkDiscreteGeometryComposerImagingExtension : public dtkComposerExtension
{
public:
    dtkDiscreteGeometryComposerImagingExtension(void);
    ~dtkDiscreteGeometryComposerImagingExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkDiscreteGeometryComposerImagingExtension.h ends here
