// Version: $Id: d0c58a963562b8af0add85fe1984c3f775982dd8 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSEREXTENSION_EXPORT dtkDiscreteGeometryComposerExtension : public dtkComposerExtension
{
public:
     dtkDiscreteGeometryComposerExtension(void);
    ~dtkDiscreteGeometryComposerExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkGeometryComposerExtension.h ends here
