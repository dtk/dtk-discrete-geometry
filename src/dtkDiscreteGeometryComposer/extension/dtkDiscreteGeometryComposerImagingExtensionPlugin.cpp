// Version: $Id$
//
//

// Commentary:
//
//

// Author : Nicolas Niclause
//
//

// Change Log:
//
//

// Code:

#include "dtkDiscreteGeometryComposerImagingExtensionPlugin.h"
#include "dtkDiscreteGeometryComposerImagingExtension.h"

#include <dtkDiscreteGeometryCore>

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension* dtkDiscreteGeometryComposerImagingCreator(void);

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


void dtkDiscreteGeometryComposerImagingExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkDiscreteGeometryImaging", dtkDiscreteGeometryComposerImagingCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkDiscreteGeometryImaging");
    extension->extend(&(dtkComposer::node::factory()));
}

void dtkDiscreteGeometryComposerImagingExtensionPlugin::uninitialize(void)
{

}


// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkDiscreteGeometryComposerImagingCreator(void)
{
    return new dtkDiscreteGeometryComposerImagingExtension();
}

//
// dtkDiscreteGeometryComposerImagingExtensionPlugin.cpp ends here
