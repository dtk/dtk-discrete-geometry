// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkDiscreteGeometryCore/dtkMeshLabeler>

#include <dtkComposer/dtkComposerNodeObject>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshLabeler : public dtkComposerNodeObject< dtkMeshLabeler >
{
public:
     dtkComposerNodeMeshLabeler(void);
    ~dtkComposerNodeMeshLabeler(void);

public:
    void run(void);

private:
    class dtkComposerNodeMeshLabelerPrivate *d;
};

//
// dtkComposerNodeMeshLabeler.h ends here
