// Version: $Id: c1e080611a900435111247d22e96914e40b6b262 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkDiscreteGeometryCore/dtkPolyhedronVolumeMesher>

#include <dtkComposer/dtkComposerNodeObject>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodePolyhedronVolumeMesher : public dtkComposerNodeObject< dtkPolyhedronVolumeMesher >
{
public:
     dtkComposerNodePolyhedronVolumeMesher(void);
    ~dtkComposerNodePolyhedronVolumeMesher(void);

public:
    void run(void);

private:
    class dtkComposerNodePolyhedronVolumeMesherPrivate *d;
};

//
// dtkComposerNodePolyhedronVolumeMesher.h ends here
