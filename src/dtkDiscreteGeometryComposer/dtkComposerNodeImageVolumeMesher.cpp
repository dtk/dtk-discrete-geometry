// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeImageVolumeMesher.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkImagingCore/dtkImage>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeImageVolumeMesherPrivate
// /////////////////////////////////////////////////////////////////

class dtkComposerNodeImageVolumeMesherPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> rcv_in_image;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeImageVolumeMesher
// /////////////////////////////////////////////////////////////////

dtkComposerNodeImageVolumeMesher::dtkComposerNodeImageVolumeMesher(void) : dtkComposerNodeObject< dtkImageVolumeMesher >(), d(new dtkComposerNodeImageVolumeMesherPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::imageVolumeMesher::pluginFactory());

    this->appendReceiver(&d->rcv_in_image);

    this->appendEmitter(&d->emt_out_mesh);
    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodeImageVolumeMesher::~dtkComposerNodeImageVolumeMesher(void)
{
    delete d;
    d = nullptr;
}

void dtkComposerNodeImageVolumeMesher::run(void)
{
    if (!d->rcv_in_image.isEmpty() ) {
        dtkImageVolumeMesher *image_volume_mesher = this->object();
        if (!image_volume_mesher) {
            dtkWarn() << Q_FUNC_INFO << "No mesher instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            d->emt_stat.clearData();

            return;
        }

        image_volume_mesher->setImage(d->rcv_in_image.constData());
        image_volume_mesher->run();

        d->emt_out_mesh.setData(image_volume_mesher->mesh());
        d->emt_stat.setData(image_volume_mesher->statistics());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The necessary input are not set. Nothing is done.";
        d->emt_out_mesh.clearData();
        d->emt_stat.clearData();
        return;
    }
}

//
// dtkComposerNodeImageVolumeMesher.cpp ends here
