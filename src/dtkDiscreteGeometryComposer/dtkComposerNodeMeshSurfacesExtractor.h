// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkMeshSurfacesExtractor.h>

class dtkComposerNodeMeshSurfacesExtractorPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshSurfacesExtractor : public dtkComposerNodeObject< dtkMeshSurfacesExtractor >
{
public:
    dtkComposerNodeMeshSurfacesExtractor(void);
    ~dtkComposerNodeMeshSurfacesExtractor(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshSurfacesExtractorPrivate *d;
};

//
// dtkComposerNodeMeshSurfacesExtractor.h ends here
