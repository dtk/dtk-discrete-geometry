// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkComposer/dtkComposerNodeObject>

#include <dtkDiscreteGeometryCore/dtkBrepVolumeMesher>

// ///////////////////////////////////////////////////////////////////
// dtkComposerNodeBrepVolumeMesher
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeBrepVolumeMesher : public dtkComposerNodeObject< dtkBrepVolumeMesher >
{
public:
     dtkComposerNodeBrepVolumeMesher(void);
    ~dtkComposerNodeBrepVolumeMesher(void);

public:
    void run(void);

private:
    class dtkComposerNodeBrepVolumeMesherPrivate *d;
};

//
// dtkComposerNodeBrepVolumeMesher.h ends here
