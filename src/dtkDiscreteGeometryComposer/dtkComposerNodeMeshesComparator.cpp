#include "dtkDiscreteGeometryCore.h"

#include "dtkComposerNodeMeshesComparator.h"

#include <dtkMesh>

class dtkComposerNodeMeshesComparatorPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_dtk_mesh_1;
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_dtk_mesh_2;

public:
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

dtkComposerNodeMeshesComparator::dtkComposerNodeMeshesComparator(void) : dtkComposerNodeObject< dtkMeshesComparator >(), d(new dtkComposerNodeMeshesComparatorPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshesComparator::pluginFactory());

    this->appendReceiver(&d->rcv_in_dtk_mesh_1);

    this->appendReceiver(&d->rcv_in_dtk_mesh_2);

    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodeMeshesComparator::~dtkComposerNodeMeshesComparator(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshesComparator::run(void)
{
    if (!d->rcv_in_dtk_mesh_1.isEmpty() && !d->rcv_in_dtk_mesh_2.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No mesh surface comparison instantiated, abort:" << this->currentImplementation();
            d->emt_stat.clearData();

            return;
        }

        dtkMesh * dtk_mesh_1 = d->rcv_in_dtk_mesh_1.constData();
        dtkMesh * dtk_mesh_2 = d->rcv_in_dtk_mesh_2.constData();
        dtkMeshesComparator *mesh_surface_comparison = this->object();
        mesh_surface_comparison->setMeshes(dtk_mesh_1, dtk_mesh_2);

        mesh_surface_comparison->run();
        d->emt_stat.setData(mesh_surface_comparison->statistics());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_stat.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshesComparator.cpp ends here
