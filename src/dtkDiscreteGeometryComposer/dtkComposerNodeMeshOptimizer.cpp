// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeMeshOptimizer.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

class dtkComposerNodeMeshOptimizerPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

dtkComposerNodeMeshOptimizer::dtkComposerNodeMeshOptimizer(void) : dtkComposerNodeObject< dtkMeshOptimizer >(), d(new dtkComposerNodeMeshOptimizerPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshOptimizer::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_mesh);

    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodeMeshOptimizer::~dtkComposerNodeMeshOptimizer(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshOptimizer::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No optimizer instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            d->emt_stat.clearData();

            return;
        }
        dtkMeshOptimizer *mesh_optimizer = this->object();
        mesh_optimizer->setMesh(d->rcv_in_mesh.constData());

        mesh_optimizer->run();
        d->emt_out_mesh.setData(mesh_optimizer->mesh());
        d->emt_stat.setData(mesh_optimizer->statistics());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_mesh.clearData();
        d->emt_stat.clearData();
        return;
    }
}

//
// dtkComposerNodeMeshOptimizer.cpp ends here
