// Version: $Id: c1e080611a900435111247d22e96914e40b6b262 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkComposer/dtkComposerNodeObject>

#include <dtkDiscreteGeometryCore/dtkMeshOptimizer>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshOptimizer : public dtkComposerNodeObject< dtkMeshOptimizer >
{
public:
     dtkComposerNodeMeshOptimizer(void);
    ~dtkComposerNodeMeshOptimizer(void);

public:
    void run(void);

private:
    class dtkComposerNodeMeshOptimizerPrivate *d;
};

//
// dtkComposerNodeMeshOptimizer.h ends here
