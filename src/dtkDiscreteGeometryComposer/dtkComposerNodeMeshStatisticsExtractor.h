// Version: $Id: c1e080611a900435111247d22e96914e40b6b262 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkMeshStatisticsExtractor.h>

class dtkComposerNodeMeshStatisticsExtractorPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshStatisticsExtractor : public dtkComposerNodeObject< dtkMeshStatisticsExtractor >
{
public:
    dtkComposerNodeMeshStatisticsExtractor(void);
    ~dtkComposerNodeMeshStatisticsExtractor(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshStatisticsExtractorPrivate *d;
};

//
// dtkComposerNodeMeshStatisticsExtractor.h ends here
