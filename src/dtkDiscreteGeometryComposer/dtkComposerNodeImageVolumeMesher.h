// Version: $Id: c1e080611a900435111247d22e96914e40b6b262 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkDiscreteGeometryCore/dtkImageVolumeMesher.h>

#include <dtkComposer/dtkComposerNodeObject>

// ///////////////////////////////////////////////////////////////////
// dtkComposerNodeImageVolumeMesher
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeImageVolumeMesher : public dtkComposerNodeObject< dtkImageVolumeMesher >
{
public:
     dtkComposerNodeImageVolumeMesher(void);
    ~dtkComposerNodeImageVolumeMesher(void);

public:
    void run(void);

private:
    class dtkComposerNodeImageVolumeMesherPrivate *d;
};

//
// dtkComposerNodeImageVolumeMesherMesher.h ends here
