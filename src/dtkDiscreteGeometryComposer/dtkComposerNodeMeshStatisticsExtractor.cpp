#include "dtkDiscreteGeometryCore.h"

#include "dtkComposerNodeMeshStatisticsExtractor.h"

#include <dtkMesh>

class dtkComposerNodeMeshStatisticsExtractorPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh*> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

dtkComposerNodeMeshStatisticsExtractor::dtkComposerNodeMeshStatisticsExtractor(void) : dtkComposerNodeObject< dtkMeshStatisticsExtractor >(), d(new dtkComposerNodeMeshStatisticsExtractorPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshStatisticsExtractor::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodeMeshStatisticsExtractor::~dtkComposerNodeMeshStatisticsExtractor(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshStatisticsExtractor::run(void)
{
    if (d->rcv_in_mesh.constData()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No mesh statistics instantiated, abort:" << this->currentImplementation();
            d->emt_stat.clearData();

            return;
        }

        dtkMeshStatisticsExtractor *mesh_statistics = this->object();
        mesh_statistics->setMesh(d->rcv_in_mesh.constData());

        mesh_statistics->run();

        d->emt_stat.setData(mesh_statistics->statistics());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input is not set. Nothing is done.";
        d->emt_stat.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshStatisticsExtractor.cpp ends here
