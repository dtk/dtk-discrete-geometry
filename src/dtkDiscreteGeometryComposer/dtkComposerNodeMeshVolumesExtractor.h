// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkMeshVolumesExtractor.h>

class dtkComposerNodeMeshVolumesExtractorPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshVolumesExtractor : public dtkComposerNodeObject< dtkMeshVolumesExtractor >
{
public:
    dtkComposerNodeMeshVolumesExtractor(void);
    ~dtkComposerNodeMeshVolumesExtractor(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshVolumesExtractorPrivate *d;
};

//
// dtkComposerNodeMeshVolumesExtractor.h ends here
