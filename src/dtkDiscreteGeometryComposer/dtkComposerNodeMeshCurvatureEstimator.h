// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkComposer/dtkComposerNodeObject>

#include <dtkDiscreteGeometryCore/dtkMeshCurvatureEstimator>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshCurvatureEstimator : public dtkComposerNodeObject< dtkMeshCurvatureEstimator >
{
public:
     dtkComposerNodeMeshCurvatureEstimator(void);
    ~dtkComposerNodeMeshCurvatureEstimator(void);

public:
    void run(void);

private:
    class dtkComposerNodeMeshCurvatureEstimatorPrivate *d;
};

//
// dtkComposerNodeMeshCurvatureEstimator.h ends here
