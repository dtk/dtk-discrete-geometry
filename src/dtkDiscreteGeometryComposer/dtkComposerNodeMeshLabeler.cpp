// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeMeshLabeler.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

class dtkComposerNodeMeshLabelerPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkMesh *> rcv_in_mesh;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
};

dtkComposerNodeMeshLabeler::dtkComposerNodeMeshLabeler(void) : dtkComposerNodeObject< dtkMeshLabeler >(), d(new dtkComposerNodeMeshLabelerPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::meshLabeler::pluginFactory());

    this->appendReceiver(&d->rcv_in_mesh);

    this->appendEmitter(&d->emt_out_mesh);
}

dtkComposerNodeMeshLabeler::~dtkComposerNodeMeshLabeler(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeMeshLabeler::run(void)
{
    if (!d->rcv_in_mesh.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No meshLabeler instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            return;
        }

        dtkMesh* mesh_in = d->rcv_in_mesh.constData();
        dtkMeshLabeler *process = this->object();
        process->setMesh(mesh_in);

        process->run();
        d->emt_out_mesh.setData(process->mesh());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_mesh.clearData();
        return;

    }
}

//
// dtkComposerNodeMeshLabeler.cpp ends here
