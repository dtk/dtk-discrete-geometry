// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDiscreteGeometryComposerExport.h>

#include <dtkComposer>

class dtkComposerNodeMeshCollectionToMeshListPrivate;

// ///////////////////////////////////////////////////////////////////
// dtkComposerNodeMeshCollectionToMeshList
// ///////////////////////////////////////////////////////////////////

class DTKDISCRETEGEOMETRYCOMPOSER_EXPORT dtkComposerNodeMeshCollectionToMeshList : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeMeshCollectionToMeshList(void);
    ~dtkComposerNodeMeshCollectionToMeshList(void);

public:
    void run(void);

private:
    dtkComposerNodeMeshCollectionToMeshListPrivate *d;
};

//
// dtkComposerNodeMeshCollectionToMeshList.h ends here
