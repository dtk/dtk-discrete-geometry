// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeBrepVolumeMesher.h"

#include <dtkDiscreteGeometryCore/dtkMesh>

#include <dtkContinuousGeometryCore/dtkBRep>

#include <dtkComposer/dtkComposerTransmitterEmitter>
#include <dtkComposer/dtkComposerTransmitterReceiver>

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeBrepVolumeMesherPrivate
// /////////////////////////////////////////////////////////////////

class dtkComposerNodeBrepVolumeMesherPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkBRep *> rcv_in_brep;

public:
    dtkComposerTransmitterEmitter<dtkMesh *> emt_out_mesh;
    dtkComposerTransmitterEmitter<QVariantMap> emt_stat;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeBrepVolumeMesher
// /////////////////////////////////////////////////////////////////

dtkComposerNodeBrepVolumeMesher::dtkComposerNodeBrepVolumeMesher(void) : dtkComposerNodeObject< dtkBrepVolumeMesher >(), d(new dtkComposerNodeBrepVolumeMesherPrivate())
{
    this->setFactory(dtkDiscreteGeometryCore::brepVolumeMesher::pluginFactory());

    this->appendReceiver(&d->rcv_in_brep);

    this->appendEmitter(&d->emt_out_mesh);
    this->appendEmitter(&d->emt_stat);
}

dtkComposerNodeBrepVolumeMesher::~dtkComposerNodeBrepVolumeMesher(void)
{
    delete d;
    d = nullptr;
}

void dtkComposerNodeBrepVolumeMesher::run(void)
{
    if (!d->rcv_in_brep.isEmpty() ) {
        dtkBrepVolumeMesher *brep_volume_mesher = this->object();
        if (!brep_volume_mesher) {
            dtkWarn() << Q_FUNC_INFO << "No mesher instantiated, abort:" << this->currentImplementation();
            d->emt_out_mesh.clearData();
            d->emt_stat.clearData();

            return;
        }

        brep_volume_mesher->setBrep(d->rcv_in_brep.constData());
        brep_volume_mesher->run();

        d->emt_out_mesh.setData(brep_volume_mesher->mesh());
        d->emt_stat.setData(brep_volume_mesher->statistics());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The necessary input are not set. Nothing is done.";
        d->emt_out_mesh.clearData();
        d->emt_stat.clearData();
        return;
    }
}

//
// dtkComposerNodeBrepVolumeMesher.cpp ends here
