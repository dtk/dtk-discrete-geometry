## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkMeshPartitionerApp)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS)

set(${PROJECT_NAME}_SOURCES
  main.cpp)

## #################################################################
## Build rules
## #################################################################

if(NOT MSVC)
  add_definitions(-Wno-write-strings)
endif(NOT MSVC)

add_executable(${PROJECT_NAME} MACOSX_BUNDLE WIN32
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

## ###################################################################
## Link rules
## ###################################################################
SET(${PROJECT_NAME}_linked_libs dtkDiscreteGeometryCore
dtkLog
dtkCore)
if(DTK_BUILD_DISTRIBUTED)
  SET(${PROJECT_NAME}_linked_libs ${${PROJECT_NAME}_linked_libs}
    dtkDiscreteGeometryDistributed)
endif()
target_link_libraries(${PROJECT_NAME} ${${PROJECT_NAME}_linked_libs})

target_link_libraries(${PROJECT_NAME} Qt5::Core)

## #################################################################
## Source file layout in development environments like Visual Studio
## #################################################################

SOURCE_GROUP("Header Files" REGULAR_EXPRESSION .*\\.h\$)
SOURCE_GROUP("Generated Files" FILES ${${PROJECT_NAME}_SOURCES_MOC})

## #################################################################
## Installation
## #################################################################

install(FILES ${${PROJECT_NAME}_MODULES} DESTINATION modules)
install(TARGETS ${PROJECT_NAME}
   BUNDLE DESTINATION bin
  RUNTIME DESTINATION bin)

######################################################################
### CMakeLists.txt ends here
