// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>

#include <dtkLog>
#include <dtkCore>
#include <dtkDiscreteGeometryCore>


class Partitioner : public QRunnable
{
public:
    QString partitioner_impl;
    QString mesh_file;
    int partition_count;

public:
    void run(void) {

        // Read input mesh
        dtkMeshReader *mesh_reader= dtkDiscreteGeometryCore::meshReader::pluginFactory().create("vtkMeshReader");

        if(!mesh_reader){
            dtkError() <<"No Reader found";
            exit(0);
        }

        mesh_reader->setMeshFile(mesh_file);
        mesh_reader->run();
        dtkMesh *mesh = mesh_reader->mesh();

        // Partition the mesh
        dtkMeshPartitioner *partitioner = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().create(this->partitioner_impl);

        if(!partitioner){
            dtkError() << "No partitioner instanciated.";
            exit(0);
        }

        dtkMeshPartitionMap map;

        partitioner->setMesh(mesh);
        partitioner->setPartitionCount(this->partition_count);
        partitioner->setMeshPartitionMap(&map);
        partitioner->run();

        for (auto i= 0ul; i< map.elementCount(); ++i) {
            dtkTrace() << i << map.elementsPartition()[i];

        }
    }
};

int main(int argc, char **argv)
{
    dtkCoreApplication application(argc, argv);
    application.setApplicationName("dtkMeshPartitionerApp");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");
    application.setApplicationVersion("1.3.2");

    QCommandLineParser *parser = application.parser();
    parser->setApplicationDescription("DTK Mesh Partitioner");

    // ------------ set options

    QCommandLineOption meshOption("mesh", "mesh file (vtk format)", "file");
    parser->addOption(meshOption);

    QCommandLineOption partitionOption("partitions", "number of partitions", "integer", "1");
    parser->addOption(partitionOption);

    QCommandLineOption partitionerImplOption("partitioner", "partitioner implementation. ", "metis|scotch|...", "metis");
    parser->addOption(partitionerImplOption);

    QCommandLineOption showPartitionersOption("show-partitioners", "show partitioner implementations available.");
    parser->addOption(showPartitionersOption);

    application.initialize();

    // ------------ initialize layers

    dtkDiscreteGeometryCoreSettings discrete_geometry_core_settings;
    discrete_geometry_core_settings.beginGroup("discrete-geometry-core");

    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));
    if (parser->isSet(verboseOption)) {
        dtkDiscreteGeometryCore::meshPartitioner::pluginManager().setVerboseLoading(true);
        dtkDiscreteGeometryCore::meshReader::pluginManager().setVerboseLoading(true);
    }
    dtkDiscreteGeometryCore::initialize(discrete_geometry_core_settings.value("plugins").toString());
    discrete_geometry_core_settings.endGroup();

    QStringList partitioners = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().keys();
    QString partitionersStr = partitioners.filter(QRegExp("^(?!.*Generic).*$")).join(" ");

    if (parser->isSet(showPartitionersOption)) {
        qDebug() << " dynamically loaded partitioners:" << partitionersStr;
        exit(0);
    }
    dtkInfo() << "Dynamicaly loaded partitioner plugins:" << partitionersStr;

    // ------------ check parameters

    if (!parser->isSet(meshOption) || !parser->isSet(partitionOption)) {
        qCritical() << "Error: no mesh or number of partition were set ! Use --mesh <filename> --partition <integer>" ;
        return 1;
    }

    Partitioner partitioner;

    // ----- Sets the partitioner

    if (parser->isSet(partitionerImplOption)) {
        if (parser->value(partitionerImplOption) == "metis") {
            partitioner.partitioner_impl = "dtkMeshPartitionerMetis";
        } else {
            qWarning() << "Unknown partitioner " << parser->value(partitionerImplOption) << "abort";
            return 1;
        }
    } else {
        partitioner.partitioner_impl = "dtkMeshPartitionerMetis";
    }

    if (!partitionersStr.contains(partitioner.partitioner_impl)) {
        qWarning() << "Partitioner " << partitioner.partitioner_impl << "is not available. Please check your plugin path. Abort.";
            return 1;
    }

    // ----- Sets parameters

    partitioner.mesh_file = parser->value(meshOption);
    partitioner.partition_count = parser->value(partitionOption).toInt();

    //
    partitioner.run();

    return 1;
}

//
// main.cpp ends here
