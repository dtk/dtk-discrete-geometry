// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkMeshReaderGenericTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMeshReaderCreation(void);
    void testSetMeshFile(void);
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkMeshReaderGenericDataTest.h ends here
