// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshTest.h"

#include <dtkDiscreteGeometryCore.h>

#include <dtkDiscreteGeometryCoreSettings.h>

#include <dtkMesh>

#include <dtkTest>

void dtkMeshTestCase::initTestCase(void)
{
    dtkDiscreteGeometryCoreSettings settings;
    settings.beginGroup("discrete-geometry-core");
    dtkDiscreteGeometryCore::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkMeshTestCase::init(void)
{
}

void dtkMeshTestCase::testMeshCreation(void)
{
    dtkMeshData *invalid_mesh_data = dtkDiscreteGeometryCore::meshData::pluginFactory().create("WrongKEY");
    QVERIFY(!invalid_mesh_data);

    dtkMeshData *valid_mesh_data = dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault");
    QVERIFY(valid_mesh_data);

    dtkMesh *mesh = new dtkMesh(valid_mesh_data);

    QVERIFY(mesh->data());
    QVERIFY(mesh);
    delete mesh;

}

void dtkMeshTestCase::testMeshPoints(void)
{
    dtkMesh *mesh = new dtkMesh(dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault"));

    dtkMeshData *mesh_data = mesh->data();
    mesh_data->setGeometricalDimension(3);
    mesh_data->setPointsCount(4);

    double coord[12] = {0,0,0,  0,0,1,  0, 1,1,  1,1,1};

    mesh_data->setPoints(coord,4);

    double check_coord[3];

    //Recover the coordinates of the first point
    mesh_data->pointCoordinates(0, check_coord);

    QCOMPARE(check_coord[0], coord[0]);
    QCOMPARE(check_coord[1], coord[1]);
    QCOMPARE(check_coord[2], coord[2]);

    //Recover the coordinates of the first point
    mesh_data->pointCoordinates(2, check_coord);

    QCOMPARE(check_coord[0], coord[6]);
    QCOMPARE(check_coord[1], coord[7]);
    QCOMPARE(check_coord[2], coord[8]);

    delete mesh;
}

void dtkMeshTestCase::testMeshCells(void) {
    dtkMesh *mesh = new dtkMesh(dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault"));

    dtkMeshData *mesh_data = mesh->data();
    mesh_data->setGeometricalDimension(3);
    mesh_data->setPointsCount(4);

    double coord[12] = {0,0,0,  0,0,1,  0, 1,1,  1,1,1};

    mesh_data->setPoints(coord,4);

    mesh_data->setCellsCount(4,0);
    mesh_data->setCellsCount(1,3);

    dtkMeshData::CellType cells_type[5];
    dtkMeshData::IdxType cells_points[13];

    for (unsigned int i=0; i<4; ++i) {
        cells_type[i]=dtkMeshData::Vertex;
        cells_points[2*i] = 1;
        cells_points[2*i+1] = i;
    };

    cells_type[4] = dtkMeshData::Tetra;
    cells_points[8] = 4;
    cells_points[9] = 0;
    cells_points[10] = 1;
    cells_points[11] = 2;
    cells_points[12] = 3;

    mesh_data->setCellsType(cells_type);
    mesh_data->setCellsPoints(cells_points, 13);

    QVERIFY(mesh->data()->cellsCount(0)==4);
    QVERIFY(mesh->data()->cellsCount(3)==1);

    for (unsigned int i=0; i<4; ++i) {
        QVERIFY(mesh->data()->cellType(i)==dtkMeshData::Vertex);
    }
    QVERIFY(mesh->data()->cellType(4)==dtkMeshData::Tetra);

    const dtkMeshData::IdxType* check_idx;
    int cell_nb_pts;

    mesh->data()->cellPoints(4, cell_nb_pts, check_idx);

    QVERIFY(cell_nb_pts==4);

    QVERIFY(check_idx[0] == 0);
    QVERIFY(check_idx[1] == 1);
    QVERIFY(check_idx[2] == 2);
    QVERIFY(check_idx[3] == 3);

    for (unsigned int i=0; i<4; ++i) {
        mesh->data()->cellPoints(i, cell_nb_pts, check_idx);

        QCOMPARE(cell_nb_pts, 1LL);
        QCOMPARE(check_idx[0], i);
    }
    dtkMeshData::CntType cells_nb = -1;
    const dtkMeshData::IdxType *cells ;
    cells = mesh->data()->cellsPoints(cells_nb, 0);
    QCOMPARE(cells_nb, 8LL);
    cells = mesh->data()->cellsPoints(cells_nb, 1);
    QCOMPARE(cells_nb, 0LL);
    cells = mesh->data()->cellsPoints(cells_nb, 2);
    QCOMPARE(cells_nb, 0LL);
    cells = mesh->data()->cellsPoints(cells_nb, 3);
    QCOMPARE(cells_nb, 4LL);
    QCOMPARE(cells[0], 4LL);
    QCOMPARE(cells[1], 0LL);
    QCOMPARE(cells[2], 1LL);
    QCOMPARE(cells[3], 2LL);
    QCOMPARE(cells[4], 3LL);

    delete mesh;
}

void dtkMeshTestCase::testMeshCellsTriangle(void) {
    dtkMesh *mesh = new dtkMesh(dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault"));

    dtkMeshData *mesh_data = mesh->data();
    mesh_data->setGeometricalDimension(3);
    mesh_data->setPointsCount(4);

    double coord[12] = {0,0,0,  0,0,1,  0, 1,1,  1,1,1};

    mesh_data->setPoints(coord,4);

    mesh_data->setCellsCount(4,0);
    mesh_data->setCellsCount(1,2);

    dtkMeshData::CellType cells_type[5];
    dtkMeshData::IdxType cells_points[12];

    for (unsigned int i=0; i<4; ++i) {
        cells_type[i]=dtkMeshData::Vertex;
        cells_points[2*i] = 1;
        cells_points[2*i+1] = i;
    };

    cells_type[4] = dtkMeshData::Triangle;
    cells_points[8] = 3;
    cells_points[9] = 0;
    cells_points[10] = 1;
    cells_points[11] = 2;

    mesh_data->setCellsType(cells_type);
    mesh_data->setCellsPoints(cells_points, 12);

    QVERIFY(mesh->data()->cellsCount(0)==4);
    QVERIFY(mesh->data()->cellsCount(2)==1);
    QVERIFY(mesh->data()->cellsCount(3)==0);

    for (unsigned int i=0; i<4; ++i) {
        QVERIFY(mesh->data()->cellType(i)==dtkMeshData::Vertex);
    }
    QVERIFY(mesh->data()->cellType(4)==dtkMeshData::Triangle);

    const dtkMeshData::IdxType* check_idx;
    int cell_nb_pts;

    mesh->data()->cellPoints(4, cell_nb_pts, check_idx);

    QVERIFY(cell_nb_pts==3);

    QVERIFY(check_idx[0] == 0);
    QVERIFY(check_idx[1] == 1);
    QVERIFY(check_idx[2] == 2);

    for (unsigned int i=0; i<4; ++i) {
        mesh->data()->cellPoints(i, cell_nb_pts, check_idx);

        QCOMPARE(cell_nb_pts, 1LL);
        QCOMPARE(check_idx[0], i);
    }
    dtkMeshData::CntType cells_nb = -1;
    const dtkMeshData::IdxType *cells ;
    cells = mesh->data()->cellsPoints(cells_nb, 0);
    QCOMPARE(cells_nb, 8LL);
    QCOMPARE(cells[0], 1LL);
    QCOMPARE(cells[1], 0LL);
    QCOMPARE(cells[2], 1LL);
    cells = mesh->data()->cellsPoints(cells_nb, 1);
    QCOMPARE(cells_nb, 0LL);
    cells = mesh->data()->cellsPoints(cells_nb, 2);
    QCOMPARE(cells_nb, 3LL);
    QCOMPARE(cells[0], 3LL);
    QCOMPARE(cells[1], 0LL);
    QCOMPARE(cells[2], 1LL);
    QCOMPARE(cells[3], 2LL);
    cells = mesh->data()->cellsPoints(cells_nb, 3);
    QCOMPARE(cells_nb, 0LL);

    delete mesh;
}

void dtkMeshTestCase::cleanupTestCase(void)
{

}

void dtkMeshTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkMeshTest, dtkMeshTestCase)

//
// dtkMeshDataTest.cpp ends here
