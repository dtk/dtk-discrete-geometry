// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkMeshTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMeshCreation(void);
    void testMeshPoints(void);
    void testMeshCells(void);
    void testMeshCellsTriangle(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkMeshDataTest.h ends here
