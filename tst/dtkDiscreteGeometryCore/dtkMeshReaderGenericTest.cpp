// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshReaderGenericTest.h"

#include <dtkDiscreteGeometryCore.h>

#include <dtkDiscreteGeometryCoreSettings.h>

#include <dtkMesh>
#include <dtkMeshReader>
#include <dtkMeshReaderGeneric>

#include <dtkTest>

#include <QFile>

void dtkMeshReaderGenericTestCase::initTestCase(void)
{
    dtkDiscreteGeometryCoreSettings settings;
    settings.beginGroup("discrete-geometry-core");
    dtkDiscreteGeometryCore::initialize(settings.value("plugins").toString());
    settings.endGroup();
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
}

void dtkMeshReaderGenericTestCase::init(void)
{
}

void dtkMeshReaderGenericTestCase::testMeshReaderCreation(void)
{
    dtkMeshReader *invalid_mesh_reader = dtkDiscreteGeometryCore::meshReader::pluginFactory().create("WrongKEY");
    QVERIFY(!invalid_mesh_reader);

    dtkMeshReader *valid_mesh_reader_without_factory = new dtkMeshReaderGeneric();
    QVERIFY(valid_mesh_reader_without_factory);
    delete valid_mesh_reader_without_factory;
}

void dtkMeshReaderGenericTestCase::testSetMeshFile(void)
{
    QFile file(QFINDTESTDATA("data/cube.off"));
    QVERIFY(file.exists());
}

void dtkMeshReaderGenericTestCase::testRun(void)
{
    dtkMeshReader *mesh_reader = new dtkMeshReaderGeneric();
    QVERIFY(mesh_reader);

    mesh_reader->setMeshFile(QFINDTESTDATA("data/cube.off"));
    mesh_reader->run();
    dtkMesh *mesh = mesh_reader->mesh();
    QVERIFY(mesh != NULL);

    dtkMeshData *mesh_data = mesh->data();
    QVERIFY(mesh_data != NULL);
    QCOMPARE(mesh_data->pointsCount()  , 8);
    QCOMPARE(mesh_data->cellsCount(2)   , 6);

    delete mesh_data;
    delete mesh_reader;
}



void dtkMeshReaderGenericTestCase::cleanupTestCase(void)
{

}

void dtkMeshReaderGenericTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkMeshReaderGenericTest, dtkMeshReaderGenericTestCase)


//
// dtkMeshReaderGenericTest.cpp ends here
