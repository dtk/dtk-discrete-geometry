// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshReaderDefaultTest.h"

#include <dtkDiscreteGeometryCore.h>

#include <dtkDiscreteGeometryCoreSettings.h>

#include <dtkMesh>
#include <dtkMeshReader>
#include <dtkMeshReaderDefault>

#include <dtkTest>

#include <QFile>

void dtkMeshReaderDefaultTestCase::initTestCase(void)
{
    dtkDiscreteGeometryCoreSettings settings;
    settings.beginGroup("discrete-geometry-core");
    dtkDiscreteGeometryCore::initialize(settings.value("plugins").toString());
    settings.endGroup();
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
}

void dtkMeshReaderDefaultTestCase::init(void)
{
}

void dtkMeshReaderDefaultTestCase::testMeshReaderCreation(void)
{
    dtkMeshReader *invalid_mesh_reader = dtkDiscreteGeometryCore::meshReader::pluginFactory().create("WrongKEY");
    QVERIFY(!invalid_mesh_reader);

    dtkMeshReader *valid_mesh_reader_with_factory = dtkDiscreteGeometryCore::meshReader::pluginFactory().create("dtkMeshReaderDefault");
    QVERIFY(valid_mesh_reader_with_factory);
    delete valid_mesh_reader_with_factory;

    dtkMeshReader *valid_mesh_reader_without_factory = new dtkMeshReaderDefault();
    QVERIFY(valid_mesh_reader_without_factory);
    delete valid_mesh_reader_without_factory;

}

void dtkMeshReaderDefaultTestCase::testSetMeshFile(void)
{
    QFile file(QFINDTESTDATA("data/cube.off"));
    QVERIFY(file.exists());
}

void dtkMeshReaderDefaultTestCase::testRun(void)
{
    dtkMeshReader *mesh_reader = dtkDiscreteGeometryCore::meshReader::pluginFactory().create("dtkMeshReaderDefault");
    QVERIFY(mesh_reader);

    // ///////////////////////////////////////////////////////////////////
    // Copies the file to obtain a valid physical adress on the disk
    // ///////////////////////////////////////////////////////////////////
    mesh_reader->setMeshFile(QFINDTESTDATA("data/cube.off"));
    mesh_reader->run();
    dtkMesh *mesh = mesh_reader->mesh();
    QVERIFY(mesh != NULL);

    dtkMeshData *mesh_data = mesh->data();
    QVERIFY(mesh_data != NULL);
    QCOMPARE(mesh_data->pointsCount()  , 8);
    QCOMPARE(mesh_data->cellsCount(2)   , 6);

    delete mesh_data;
    delete mesh_reader;
}


void dtkMeshReaderDefaultTestCase::cleanupTestCase(void)
{

}

void dtkMeshReaderDefaultTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkMeshReaderDefaultTest, dtkMeshReaderDefaultTestCase)

//
// dtkMeshReaderDefaultTest.cpp ends here
