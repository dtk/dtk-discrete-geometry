// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshPartitionMapTest.h"

#include <dtkDiscreteGeometryCore.h>

#include <dtkMeshPartitionMap>

#include <dtkLog>

#include <vector>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkMeshPartitionMapTestCasePrivate
{
public:
    std::size_t partition_count;
    std::vector<long int> elmt_parts;
    std::vector<long int> vert_parts;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMeshPartitionMapTestCase::dtkMeshPartitionMapTestCase(void) : d(new dtkMeshPartitionMapTestCasePrivate)
{

}

dtkMeshPartitionMapTestCase::~dtkMeshPartitionMapTestCase(void)
{
    delete d;
}

void dtkMeshPartitionMapTestCase::initTestCase(void)
{
    d->partition_count = 4UL;
    d->elmt_parts.resize(16UL);
    d->vert_parts.resize(13UL);

    d->elmt_parts[ 0] = 3;
    d->elmt_parts[ 1] = 2;
    d->elmt_parts[ 2] = 3;
    d->elmt_parts[ 3] = 3;
    d->elmt_parts[ 4] = 0;
    d->elmt_parts[ 5] = 1;
    d->elmt_parts[ 6] = 2;
    d->elmt_parts[ 7] = 0;
    d->elmt_parts[ 8] = 0;
    d->elmt_parts[ 9] = 2;
    d->elmt_parts[10] = 1;
    d->elmt_parts[11] = 1;
    d->elmt_parts[12] = 2;
    d->elmt_parts[13] = 1;
    d->elmt_parts[14] = 3;
    d->elmt_parts[15] = 0;

    d->vert_parts[ 0] = 2;
    d->vert_parts[ 1] = 1;
    d->vert_parts[ 2] = 0;
    d->vert_parts[ 3] = 3;
    d->vert_parts[ 4] = 1;
    d->vert_parts[ 5] = 0;
    d->vert_parts[ 6] = 0;
    d->vert_parts[ 7] = 3;
    d->vert_parts[ 8] = 0;
    d->vert_parts[ 9] = 2;
    d->vert_parts[10] = 0;
    d->vert_parts[11] = 1;
    d->vert_parts[12] = 2;
}

void dtkMeshPartitionMapTestCase::init(void)
{
}

void dtkMeshPartitionMapTestCase::testRead(void)
{
    dtkMeshPartitionMap map;
    QVERIFY(map.read("partition.txt"));

    QCOMPARE(d->partition_count, map.partitionCount());
    QCOMPARE(d->elmt_parts.size(), map.elementCount());
    QCOMPARE(d->vert_parts.size(), map.vertexCount());

    auto *elmt_parts = map.elementsPartition();
    for (std::size_t i = 0; i < map.elementCount(); ++i) {
        QCOMPARE(d->elmt_parts[i], elmt_parts[i]);
    }

    auto *vert_parts = map.verticesPartition();
    for (std::size_t i = 0; i < map.vertexCount(); ++i) {
        QCOMPARE(d->vert_parts[i], vert_parts[i]);
    }
}

void dtkMeshPartitionMapTestCase::testWrite(void)
{
    dtkMeshPartitionMap map;
    map.setPartitionCount(d->partition_count);
    map.setElementCount(d->elmt_parts.size());
    map.setVertexCount(d->vert_parts.size());

    auto *elmt_parts = map.elementsPartition();
    for (std::size_t i = 0; i < d->elmt_parts.size(); ++i) {
        elmt_parts[i] = d->elmt_parts[i];
    }

    auto *vert_parts = map.verticesPartition();
    for (std::size_t i = 0; i < map.vertexCount(); ++i) {
        vert_parts[i] = d->vert_parts[i];
    }

    QVERIFY(map.write("result.txt"));

    dtkMeshPartitionMap res;
    res.read("result.txt");

    QCOMPARE(d->partition_count, res.partitionCount());
    QCOMPARE(d->elmt_parts.size(), res.elementCount());
    QCOMPARE(d->vert_parts.size(), res.vertexCount());

    elmt_parts = res.elementsPartition();
    for (std::size_t i = 0; i < res.elementCount(); ++i) {
        QCOMPARE(d->elmt_parts[i], elmt_parts[i]);
    }

    vert_parts = res.verticesPartition();
    for (std::size_t i = 0; i < res.vertexCount(); ++i) {
        QCOMPARE(d->vert_parts[i], vert_parts[i]);
    }
}

void dtkMeshPartitionMapTestCase::cleanupTestCase(void)
{
}

void dtkMeshPartitionMapTestCase::cleanup(void)
{
}

DTKTEST_MAIN_NOGUI(dtkMeshPartitionMapTest, dtkMeshPartitionMapTestCase)

//
// dtkMeshPartitionMapTest.cpp ends here
