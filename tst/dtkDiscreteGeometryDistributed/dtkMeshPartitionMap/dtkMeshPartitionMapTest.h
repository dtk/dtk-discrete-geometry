// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// dtkMeshPartitionMapTestCase
// ///////////////////////////////////////////////////////////////////

class dtkMeshPartitionMapTestCase : public QObject
{
    Q_OBJECT

public:
     dtkMeshPartitionMapTestCase(void);
    ~dtkMeshPartitionMapTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRead(void);
    void testWrite(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkMeshPartitionMapTestCasePrivate *d;
};

//
// dtkMeshPartitionMapTest.h ends here
