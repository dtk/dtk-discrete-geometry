// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistributedMeshTest.h"
#include "dtkDistributedMeshTest_p.h"
#include "dtkDistributedMeshTestRunnable.h"

#include <dtkDiscreteGeometryCore.h>
#include <dtkDiscreteGeometryCoreSettings.h>
#include <dtkMeshPartitionMap>
#include <dtkMesh>
#include <dtkMeshReaderGeneric>
#include <dtkDiscreteGeometryCoreSettings.h>

#include <dtkDistributed>

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkDistributedMeshTestCase::dtkDistributedMeshTestCase(void) : d(new dtkDistributedMeshTestCasePrivate)
{
    d->map = new dtkMeshPartitionMap;
    d->mesh = nullptr;

    d->connectivity_topoDim_to_zero.resize(16);
    for (auto& vec : d->connectivity_topoDim_to_zero) {
        vec.resize(3);
    }

    d->connectivity_topoDim_to_zero[ 0] = {0, 1, 2};
    d->connectivity_topoDim_to_zero[ 1] = {1, 3, 4};
    d->connectivity_topoDim_to_zero[ 2] = {1, 2, 4};
    d->connectivity_topoDim_to_zero[ 3] = {0, 1, 3};
    d->connectivity_topoDim_to_zero[ 4] = {5, 6, 7};
    d->connectivity_topoDim_to_zero[ 5] = {4, 6, 7};
    d->connectivity_topoDim_to_zero[ 6] = {2, 4, 6};
    d->connectivity_topoDim_to_zero[ 7] = {2, 5, 6};
    d->connectivity_topoDim_to_zero[ 8] = {4, 8, 9};
    d->connectivity_topoDim_to_zero[ 9] = {3, 4, 8};
    d->connectivity_topoDim_to_zero[10] = {4, 7, 9};
    d->connectivity_topoDim_to_zero[11] = {7, 9, 10};
    d->connectivity_topoDim_to_zero[12] = {8, 9, 11};
    d->connectivity_topoDim_to_zero[13] = {3, 8, 12};
    d->connectivity_topoDim_to_zero[14] = {9, 10, 11};
    d->connectivity_topoDim_to_zero[15] = {8, 11, 12};


    d->connectivity_zero_to_topoDim.resize(13);
    d->connectivity_zero_to_topoDim[ 0].resize(2);
    d->connectivity_zero_to_topoDim[ 1].resize(4);
    d->connectivity_zero_to_topoDim[ 2].resize(4);
    d->connectivity_zero_to_topoDim[ 3].resize(4);
    d->connectivity_zero_to_topoDim[ 4].resize(7);
    d->connectivity_zero_to_topoDim[ 5].resize(2);
    d->connectivity_zero_to_topoDim[ 6].resize(4);
    d->connectivity_zero_to_topoDim[ 7].resize(4);
    d->connectivity_zero_to_topoDim[ 8].resize(5);
    d->connectivity_zero_to_topoDim[ 9].resize(5);
    d->connectivity_zero_to_topoDim[10].resize(2);
    d->connectivity_zero_to_topoDim[11].resize(3);
    d->connectivity_zero_to_topoDim[12].resize(2);

    d->connectivity_zero_to_topoDim[ 0] = {0, 3};
    d->connectivity_zero_to_topoDim[ 1] = {0, 1, 2, 3};
    d->connectivity_zero_to_topoDim[ 2] = {0, 2, 6, 7};
    d->connectivity_zero_to_topoDim[ 3] = {1, 3, 9, 13};
    d->connectivity_zero_to_topoDim[ 4] = {1, 2, 5, 6, 8, 9 ,10};
    d->connectivity_zero_to_topoDim[ 5] = {4, 7};
    d->connectivity_zero_to_topoDim[ 6] = {4, 5, 6, 7};
    d->connectivity_zero_to_topoDim[ 7] = {4, 5, 10, 11};
    d->connectivity_zero_to_topoDim[ 8] = {8, 9, 12, 13, 15};
    d->connectivity_zero_to_topoDim[ 9] = {8, 10, 11, 12, 14};
    d->connectivity_zero_to_topoDim[10] = {11, 14};
    d->connectivity_zero_to_topoDim[11] = {12, 14, 15};
    d->connectivity_zero_to_topoDim[12] = {13, 15};


    d->coord.resize(13);
    for (int i = 1; i < d->coord.size(); ++i) {
        d->coord[i].resize(3);
    }

    d->coord[ 0] = {1, 1, 0};
    d->coord[ 1] = {0.75, 0.75, 0};
    d->coord[ 2] = {1, 0.5, 0};
    d->coord[ 3] = {0.5, 1, 0};
    d->coord[ 4] = {0.5, 0.5, 0};
    d->coord[ 5] = {1, 0, 0};
    d->coord[ 6] = {0.75, 0.25, 0};
    d->coord[ 7] = {0.5, 0, 0};
    d->coord[ 8] = {0.25, 0.75, 0};
    d->coord[ 9] = {0.25, 0.25, 0};
    d->coord[10] = {0, 0, 0};
    d->coord[11] = {0, 0.5, 0};
    d->coord[12] = {0, 1, 0};

}


dtkDistributedMeshTestCase::~dtkDistributedMeshTestCase(void)
{
    delete d->map;
    if (d->mesh) {
        delete d->mesh;
    }
    delete d;
    d = nullptr;
}

void dtkDistributedMeshTestCase::initTestCase(void)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    dtkDiscreteGeometryCore::initialize();

    dtkMeshReaderGeneric reader;

    QFileInfo mesh_file(QFINDTESTDATA("data/simple_mesh.vtk"));
    QVERIFY(mesh_file.exists());

    reader.setMeshFile(mesh_file.absoluteFilePath());
    reader.run();
    d->mesh = reader.mesh();

    QVERIFY(d->mesh != nullptr);

    QFileInfo partition_file(QFINDTESTDATA("data/partition.txt"));
    QVERIFY(partition_file.exists());

    d->map->read(partition_file.absoluteFilePath());

    dtkDistributed::policy()->setType("qthread");

    for (int i = 0; i < 3; ++i) {
        dtkDistributed::policy()->addHost("localhost");
    }

    dtkDistributed::spawn();


}

void dtkDistributedMeshTestCase::init(void)
{
}

void dtkDistributedMeshTestCase::testBasics(void)
{

    QRunnable *test = new testRunnableBasics(d);
    dtkDistributed::exec(test);
    delete test;
}

void dtkDistributedMeshTestCase::testCoordinates(void)
{
    QRunnable *test = new testRunnableCoordinates(d);
    dtkDistributed::exec(test);
    delete test;
}

void dtkDistributedMeshTestCase::testTopoDimToZero(void)
{

    QRunnable *test = new testRunnableTopoDimToZero(d);
    dtkDistributed::exec(test);
    delete test;
}

void dtkDistributedMeshTestCase::testZeroToTopoDim(void)
{

    QRunnable *test = new testRunnableZeroToTopoDim(d);
    dtkDistributed::exec(test);
    delete test;
}

void dtkDistributedMeshTestCase::cleanupTestCase(void)
{
    dtkDistributed::unspawn();
}

void dtkDistributedMeshTestCase::cleanup(void)
{
}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedMeshTest, dtkDistributedMeshTestCase)

//
// dtkDistributedMeshTest.cpp ends here
