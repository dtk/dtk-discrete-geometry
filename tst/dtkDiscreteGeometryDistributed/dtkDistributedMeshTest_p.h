// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkMeshPartitionMap>
#include <dtkMesh>

#include <vector>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkDistributedMeshTestCasePrivate
{
public:
    dtkMesh *mesh;
    dtkMeshPartitionMap *map;

    std::vector< std::vector<long long int> > connectivity_topoDim_to_zero;
    std::vector< std::vector<long long int> > connectivity_zero_to_topoDim;
    std::vector< std::vector<double> > coord;
};

//
// dtkDistributedMeshTest_p.h ends here
