// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedMeshTest
// ///////////////////////////////////////////////////////////////////

class dtkDistributedMeshTestCase : public QObject
{
    Q_OBJECT

public:
     dtkDistributedMeshTestCase(void);
    ~dtkDistributedMeshTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testBasics(void);
    void testCoordinates(void);
    void testTopoDimToZero(void);
    void testZeroToTopoDim(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkDistributedMeshTestCasePrivate *d;
};

//
// dtkDistributedMeshTest.h ends here
