// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDistributed>

#include <dtkMesh.h>
#include <dtkMeshPartitionMap.h>

#include <dtkDistributedMesh>

#include <QtCore>
#include <QTest>
#include "dtkDistributedMeshTest_p.h"

// ///////////////////////////////////////////////////////////////////

std::size_t operator "" _sz (unsigned long long int x)
{
    return x;
}

// ///////////////////////////////////////////////////////////////////

class testRunnableBasics : public QRunnable
{
    dtkDistributedMeshTestCasePrivate *d;

public:
    testRunnableBasics(dtkDistributedMeshTestCasePrivate *dptr) : d(dptr) {}

public:
    void run(void) {
        dtkDistributedCommunicator *comm = dtkDistributed::app()->communicator();
        qlonglong wid = comm->wid();

        dtkDistributedMesh pmesh(d->mesh, d->map);

        QCOMPARE(pmesh.topologicalDimension(), 2_sz);
        QCOMPARE(pmesh.geometricalDimension(), 3_sz);

        QCOMPARE(pmesh.vertexCount(), 13_sz);
        QCOMPARE(pmesh.cellCount(), 16_sz);

        QCOMPARE(pmesh.entityCount(0_sz), 13_sz);
        QCOMPARE(pmesh.entityCount(2_sz), 16_sz);
    }
};

// ///////////////////////////////////////////////////////////////////

class testRunnableCoordinates : public QRunnable
{
    dtkDistributedMeshTestCasePrivate *d;

public:
    testRunnableCoordinates(dtkDistributedMeshTestCasePrivate *dptr) : d(dptr) {}

public:
    void run(void) {
        dtkDistributedCommunicator *comm = dtkDistributed::app()->communicator();
        qlonglong wid = comm->wid();

        dtkDistributedMesh pmesh(d->mesh, d->map);

        pmesh.coordinates()->rlock();
        for (std::size_t i = 0; i < pmesh.vertexCount() ; ++i) {
            QCOMPARE(pmesh.coordinates()->at(3 * i + 0), d->coord[i][0]);
            QCOMPARE(pmesh.coordinates()->at(3 * i + 1), d->coord[i][1]);
            QCOMPARE(pmesh.coordinates()->at(3 * i + 2), d->coord[i][2]);
        }
        pmesh.coordinates()->unlock();
    }
};

// ///////////////////////////////////////////////////////////////////

class testRunnableTopoDimToZero : public QRunnable
{
    dtkDistributedMeshTestCasePrivate *d;

public:
    testRunnableTopoDimToZero(dtkDistributedMeshTestCasePrivate *dptr) : d(dptr) {}

public:
    void run(void) {
        dtkDistributedCommunicator *comm = dtkDistributed::app()->communicator();
        qlonglong wid = comm->wid();

        dtkDistributedMesh pmesh(d->mesh, d->map);
        std::size_t topo_dim = pmesh.topologicalDimension();

        // Test connectivity [topo_dim][0]

        const auto& connect_test = pmesh.connectivity(topo_dim, 0_sz);
        const auto& connect_ref  = d->connectivity_topoDim_to_zero;

        std::size_t connection_count = 0;
        for (const auto& vec : connect_ref) {
            connection_count += vec.size();
        }

        QCOMPARE(connect_test.size(), connection_count);

        // dtkDistributedMeshEntity iterators
        auto it  = connect_test.begin();
        auto end = connect_test.end();

        std::size_t offset = connect_test.graph()->mapper()->firstIndex(wid);
        auto vvec = connect_ref.cbegin() + offset;

        // Loop over all entity of highest topological dimension
        for (; it != end; ++it, ++offset, ++vvec) {
            auto vec = *vvec;
            auto size = it.connectionCount();
            QCOMPARE(size, vec.size());

            // As connections are not necessary sorted into ascending
            // order, we first sort the connections
            auto vit  = it.begin();
            auto vend = it.end();
            std::vector<long long int> tmp(size);
            for (int i = 0; vit != vend; ++vit, ++i) {
                tmp[i] = *vit;
            }
            std::sort(tmp.begin(), tmp.end());

            // Now, we can do the comparison.
            auto tmp_it  = tmp.begin();
            auto tmp_end = tmp.end();
            for (int i = 0; tmp_it != tmp_end; ++tmp_it, ++i) {
                QCOMPARE(*tmp_it, vec[i]);
            }
        }
    }
};

// ///////////////////////////////////////////////////////////////////

class testRunnableZeroToTopoDim : public QRunnable
{
    dtkDistributedMeshTestCasePrivate *d;

public:
    testRunnableZeroToTopoDim(dtkDistributedMeshTestCasePrivate *dptr) : d(dptr) {}

public:
    void run(void) {

        // Test connectivity [0][topo_dim]
        dtkDistributedCommunicator *comm = dtkDistributed::app()->communicator();
        qlonglong wid = comm->wid();

        dtkDistributedMesh pmesh(d->mesh, d->map);
        std::size_t topo_dim = pmesh.topologicalDimension();

        const auto& connect_test = pmesh.connectivity(0_sz, topo_dim);
        const auto& connect_ref  = d->connectivity_zero_to_topoDim;

        std::size_t connection_count = 0;
        for (const auto& vec : connect_ref) {
            connection_count += vec.size();
        }
        QCOMPARE(connect_test.size(), connection_count);

        // dtkDistributedMeshEntity iterators
        auto it  = connect_test.begin();
        auto end = connect_test.end();

        std::size_t offset = connect_test.graph()->mapper()->firstIndex(wid);
        auto vvec = connect_ref.cbegin() + offset;

        // Loop over all entity of zero topological dimension
        for (; it != end; ++it, ++offset, ++vvec) {
            auto vec = *vvec;
            auto size = it.connectionCount();
            QCOMPARE(size, vec.size());

            // As connections are not necessary sorted into ascending
            // order, we first sort the connections
            auto vit  = it.begin();
            auto vend = it.end();
            std::vector<long long int> tmp(size);
            for (int i = 0; vit != vend; ++vit, ++i) {
                tmp[i] = *vit;
            }
            std::sort(tmp.begin(), tmp.end());

            // Now, we can do the comparison.
            auto tmp_it  = tmp.begin();
            auto tmp_end = tmp.end();
            for (int i = 0; tmp_it != tmp_end; ++tmp_it, ++i) {
                QCOMPARE(*tmp_it, vec[i]);
            }
        }
    }
};

//
// dtkDistributedMeshTestRunnable.h ends here
