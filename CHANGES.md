# ChangLog
## 1.3.3 - 2018-03-14
- really fix RPATH properties for composer extension
## 1.3.1 - 2018-03-09
- fix target properties for composer extension
## 1.3.0 - 2018-03-08
- use new widget factory of dtk 1.4.0
- add python wrapping
- numerous API changes
